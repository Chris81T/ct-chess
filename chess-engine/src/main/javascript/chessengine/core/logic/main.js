/**
 * Created by christian on 17.12.2015.
 */
function createLogic(_, finals, board, kingMod, queenMod, rookMod, bishopMod, knightMod, pawnMod, commonMod) {
    
    /**
     * will return relevant module, that knows the possibilities of a concrete
     * figure type
     */
    function getFigureMod(figure) {
        
        var figureType = figure.figureType;
        
        switch(figureType) {
            case finals.figureTypes.PAWN    : return pawnMod;
            case finals.figureTypes.ROOK    : return rookMod;
            case finals.figureTypes.KNIGHT  : return knightMod;
            case finals.figureTypes.BISHOP  : return bishopMod;
            case finals.figureTypes.QUEEN   : return queenMod;
            case finals.figureTypes.KING    : return kingMod;
        }
        
        throw "Logic::getFigureMod - Needed figure module not available for type = " + figureType;
        return undefined;
        
    }

    function setToGameOver(handle) {
        handle.gameOver = true;
    }

    function isGameOver(handle) {
        return handle.gameOver;
    }

    var logic = {

        newGame: function(handle) {

            function enrichHandle() {
                handle.gameOver = false;
            }

            enrichHandle();
        },

        /**
         * check, if game is checkmate or checked or in a deadlock for given colorType
         * @param handle
         * @param colorType
         * @param kingsField
         */
        checkGameState: function(handle, colorType) {

            /**
             * will check, if any other friendly figure is able to move to another field
             * @returns {boolean}
             */
            function friendlyFigureMovePossible() {
                var friendlyOccupiedFields = board.getOccupiedFields(handle, colorType);
                var possibleMoveFound = false;

                _.every(friendlyOccupiedFields, function(friendlyOccupiedField) {
                    var possibleMoves = logic.possibleMoves(handle, friendlyOccupiedField);
                    if (possibleMoves.length > 0) {
                        // this figure is able to move, so abort this checkup
                        possibleMoveFound = true;
                    }
                    return !possibleMoveFound;
                });

                return possibleMoveFound;
            }

            var gameState = finals.gameStateTypes.NORMAL;

            var kingsField = board.getKingsField(handle, colorType);

            var isChecked = logic.isChecked(handle, kingsField, true);

            if (isChecked) {
                // is also checkmate?
                var possibleKingMoves = logic.possibleMoves(handle, kingsField);

                var kingIsCaptured = possibleKingMoves.length === 0;
                var checkmate = false;

                /**
                 * possibly another friendly figure can prevent a checkmate?
                 *
                 * Use the condition to prevent obsolete - a bit expensive - checkups
                 */
                if (kingIsCaptured) {
                    var anotherFigureCanHelp = friendlyFigureMovePossible();
                    console.log('app/logic/main - King is captured. Check if other friendly figures can prevent the ' +
                        'checked state. anotherFigureCanHelp = ', anotherFigureCanHelp);

                    /**
                     * if no other figure can help, it is checkmate
                     */
                    checkmate = !anotherFigureCanHelp;
                }

                if (checkmate) {
                    gameState = finals.gameStateTypes.CHECKMATE;
                    setToGameOver(handle);
                } else {
                    gameState = finals.gameStateTypes.CHECK;
                }
            } else {
                // does a deadlock exists?

                console.log('app/logic/main - Deadlock checkup');
                var noDeadlock = friendlyFigureMovePossible();

                if (!noDeadlock) {
                    console.log('app/logic/main - Deadlock detected. No friendly figure is able to move to any position');
                    gameState = finals.gameStateTypes.DEADLOCK;
                    setToGameOver(handle);
                }
            }

            return gameState;

        },

        /**
         * check, if given kingsField can be checked by any enemy figure
         * @param handle
         * @param kingsField
         * @param ignoreFinalMovesCheckup OPTIONAL! Normally it can be undefined! Check the
         *        common mod -> finalPossibleMovesCheckup documentation / code. There it is the only position, where
         *        it is relevant and the flag is set to true.
         */
        isChecked: function(handle, kingsField, ignoreFinalMovesCheckup) {
            var king = kingsField.figure;
            var enemyOccupiedFields = board.getEnemyOccupiedFields(handle, king.colorType);

            var canBeChecked = false;

            _.every(enemyOccupiedFields, function(enemyField) {
                var figure = enemyField.figure;
                var figureMod = getFigureMod(figure);
                canBeChecked = figureMod.canCheck(handle, figure, enemyField, kingsField, logic, ignoreFinalMovesCheckup);
                return !canBeChecked;
            });

            return canBeChecked;
        },

        /**
         * create an array of possible move coordinates. If no movement is 
         * possible, the array will be empty.
         * 
         * @param {type} handle
         * @param {type} field
         * @returns {undefined}
         */
        possibleMoves: function(handle, field) {
            
            var moves = [];

            // first detect, if a figure is given. If yes, get the appropriate logic
            var figure = field.figure;
            if (figure === undefined) return moves;
            
            var figureMod = getFigureMod(figure);
            figureMod.possibleMoves(handle, figure, field, moves, this);
            
            return moves;
        },

        /**
         * will perform the movement from figure (source field) to destination field. The figure itself has to move to
         * the new field. Possibly it is a special move with more movements or chances like pawn transformation
         * @param handle
         * @param sourceField
         * @param destField
         * @return a result object with details, what happened. Check common (mod) to get more details about the result
         *         object structure
         */
        performMoveTo: function(handle, sourceField, destField) {
            console.log('app/logic - performMoveTo: sourceField = ', sourceField, ', destField = ', destField);
            var figureMod = getFigureMod(sourceField.figure);
            var result = commonMod.createMoveToResult();

            if (isGameOver(handle)) {
                throw 'Logic::performMoveTo - Game is over. Check the last given game state!';
            }

            /**
             * this function is only called, if movement is valid. Set this here globally to DRY in the mods
             */
            result.resultType = finals.moveToResult.OK;
            result.colorType = sourceField.figure.colorType;
            result.fromField = sourceField;
            result.toField = destField;
            figureMod.performMoveTo(handle, sourceField, destField, result);
            return result;
        }


    };

    return logic;

};