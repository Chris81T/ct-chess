/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createCommonMod(_, board, finals) {

    /**
     * DRY for the common getFields methods
     *
     * @param fields
     * @param sourceFigure
     * @param x
     * @param y
     * @param xOffset this is relevant for the concrete getFields method
     * @param yOffset this is relevant for the concrete getFields method
     * @returns {Array}
     */
    function commonGetFields(fields, sourceFigure, x, y, xOffset, yOffset) {
        var foundFields = [];

        function parse(x, y) {
            var field = mod.getFieldSafely(fields, x, y);
            if (field !== undefined && field.isEmpty()) {
                foundFields.push(field);
                parse(x+xOffset, y+yOffset);
            } else if (field !== undefined && field.figure.colorType !== sourceFigure.colorType) {
                /**
                 * only recognize field, if an enemy figure has occupied the field
                 */
                foundFields.push(field);
            }
        }

        parse(x+xOffset, y+yOffset);

        return foundFields;
    }

    var mod = {

        getFieldSafely: function(fields, x, y) {
            if (x < 1 || x > 8 || y < 1 || y > 8) return undefined;
            return fields[x][y];
        },

        appendFields: function (moves, fields) {
            _.each(fields, function(field) {
                moves.push(field);
            });
        },

        /**
         * compares the two given fields, if they are the same or not
         * @param fieldA
         * @param fieldB
         */
        theSame: function(fieldA, fieldB) {
            return fieldA.x === fieldB.x && fieldA.y === fieldB.y;
        },

        /**
         * useful for the canCheck procedures. Check, if field is part of fields
         */
        containsField: function(sourceField, fields) {
            var result = false;
            _.every(fields, function(field) {
                result = mod.theSame(sourceField, field);
                return !result;
            });
            return result;
        },

        /**
         * the client has to fill out the attributes. It is placed here, while the according figure mod can create / fill
         * only the result.
         */
        createMoveToResult: function() {
            return {
                /**
                 * knows, if the movement has failed or is ok or a decision is requested, before the movement can be
                 * completed
                 */
                resultType: finals.moveToResult.UNKNOWN,
                /**
                 * knows the color type of the current moved figure
                 */
                colorType: null,
                /**
                 * if a transformation is possible, the relevant data will hold here
                 */
                pawnTransformation: {
                    possible: false,
                    pawnFigure: undefined,
                    /**
                     * this is the result of the decision, the client has to make (user or automatically - remember
                     * loading a game)
                     */
                    newFigureType: undefined,
                    /**
                     * the engine will check, if relevant, the newFigureType, create the needed figure and swap it with
                     * the existing pawn. After that the new created figure will be set here, so the client has a
                     * simply directly access to it
                     */
                    newFigure: undefined
                },
                /**
                 * if a castling is performed, it will be recognized here. For that the pgnTypes values will be used.
                 * Check the finals.
                 */
                castlingType: undefined,
                /**
                 * source field
                 */
                fromField: null,
                /**
                 * destination field
                 */
                toField: null,
                /**
                 * if a figure is hit, the reference will be set here
                 */
                hitFigure: undefined,
                
                /**
                 * is the according field of the hitFigure. Remember the en passant.
                 * There the hitFigureField is not equal with the toField
                 */
                hitFigureField: undefined,
                
                /**
                 * knows the moved figure - for castling only the king will be recognized here
                 */
                movedFigure: undefined
            };
        },

        /**
         * Typically each special mod will determine all possible moves. Finally it must be checked, if the own king
         * will be checked for some of the moves. The other way is, that some of the possible moves can prevent the
         * checked state.
         *
         * NOTE: To avoid recursive infinite possibleMoves calls, this method must only be called by the origin figure
         * mod. Remember the according figure is to that time the only figure, that will be moved. This means, that
         * an enemy figure must not check, if may be the own king will be checked, because that enemy figure WONT move
         * to that time. It is a simulation triggered by this function.
         *
         * The aim is to identify, if some other enemy figures will actual check the own king, if the given figure
         * (param) will move to one of the given moves/fields (param).
         *
         * @param handle
         * @param figure
         * @param moves
         * @param logic
         * @param finalMovesCheckup if this flag is undefined, ignore it. But if it is set to true prevent the complete
         *        procedure here!
         */
        finalPossibleMovesCheckup: function(handle, figure, field, moves, logic, finalMovesCheckup) {

            function setFigureToSimulatedField(simulatedField) {
                // will return a may hit figure. Is needed to restore the origin state
                return board.moveFigure(field, simulatedField, true);
            }

            function setFigureToOriginField(simulatedField, mayHitFigure) {
                board.moveFigure(simulatedField, field, true);

                // set the may hit figure back to the origin field
                simulatedField.figure = mayHitFigure;
            }

            /**
             * will prevent recursive endless loop call
             */
            if (finalMovesCheckup === true) return;

            var kingsField = board.getKingsField(handle, figure.colorType);

            var figureIsKing = figure.figureType === finals.figureTypes.KING;

            var movesToRemove = [];

            _.each(moves, function(move) {

                /**
                 * perform virtually the move and check the king's situation. If the king is not checked, the move is
                 * definitely possible
                 *
                 * remember: the move is the field
                 */
                var mayHitFigure = setFigureToSimulatedField(move);

                /**
                 * Possibly the figure is the king itself. in that case instead of the kingsField, that wont have the
                 * king in simulation mode, the move (field) itself has to be used.
                 */
                if (logic.isChecked(handle, figureIsKing ? move : kingsField, true)) {
                    movesToRemove.push(move);
                }

                setFigureToOriginField(move, mayHitFigure);

            });

            /**
             * After parsing all moves it is possible to remove the not allowed moves, that can be removed.
             */
            _.each(movesToRemove, function(moveToRemove) {
                var movesIndex = moves.indexOf(moveToRemove);
                if (movesIndex > -1) {
                    moves.splice(movesIndex, 1);
                } else {
                    throw 'CommonMod::finalPossibleMovesCheckup move not part of moves!';
                }
            });

        },

        /**
         * the most of the figures wont have any special procedures like the king (castling) or the pawn (pawn
         * transformation) and can simply use this one.
         */
        performMoveTo: function(handle, sourceField, destField, moveToResult) {
            var figure = sourceField.figure;

            // if some figure exists on the destination field, place the reference to the result
            moveToResult.hitFigure = destField.figure;
            if (moveToResult.hitFigure !== undefined) {
                moveToResult.hitFigureField = destField;
            }
            
            // set the moved figure. Is relevant for the epgn
            moveToResult.movedFigure = figure;

            board.moveFigure(sourceField, destField);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getUpperFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, 0, 1);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getLowerFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, 0, -1);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getLeftFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, -1, 0);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getRightFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, 1, 0);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getUpperLeftFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, -1, 1);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getUpperRightFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, 1, 1);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getLowerLeftFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, -1, -1);
        },

        /**
         * @return an array of fields in respective direction including may first occupied field (possibly it is an
         * enemy figure, that maybe can be attacked)
         */
        getLowerRightFields: function(fields, sourceFigure, x, y) {
            return commonGetFields(fields, sourceFigure, x, y, 1, -1);
        }

    };

    return mod;

};


