/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createRookMod(common, finals) {
   
    /**
     * Each mod must prevent the same signature of methods. So ensure to have
     * the same structure like an interface for the logic module.
     */   
    var mod = {

        possibleMoves: function(handle, figure, field, moves, logic, ignoreFinalMovesCheckup) {

            var x = field.x;
            var y = field.y;

            var fields = handle.fields;

            common.appendFields(moves, common.getLeftFields(fields, figure, x, y));
            common.appendFields(moves, common.getUpperFields(fields, figure, x, y));
            common.appendFields(moves, common.getRightFields(fields, figure, x, y));
            common.appendFields(moves, common.getLowerFields(fields, figure, x, y));

            common.finalPossibleMovesCheckup(handle, figure, field, moves, logic, ignoreFinalMovesCheckup);
        },

        canCheck: function(handle, figure, field, kingsField, logic, ignoreFinalMovesCheckup) {
            var possibleFields = [];
            this.possibleMoves(handle, figure, field, possibleFields, logic, ignoreFinalMovesCheckup);
            return common.containsField(kingsField, possibleFields);
        },

        performMoveTo: function(handle, sourceField, destField, moveToResult) {
            common.performMoveTo(handle, sourceField, destField, moveToResult);
        }

    };
   
    return mod;   
    
    
};


