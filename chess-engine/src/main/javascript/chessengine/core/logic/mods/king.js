/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createKingMod(common, finals, board) {

    /**
     * it is needed to make this part DRY like. At the end the king has to check, if any other enemy figure can check
     * some of the possible fields. It is not allowed to use the whole possibleMoves function. Remember, that the
     * enemy part also have a king. This will ends in a infinite loop of calls.
     */
    function checkBasicPossibleMoves(handle, figure, field, moves, logic) {

        function checkField(x, y, fields, possibleFields) {
            var field = common.getFieldSafely(fields, x, y);
            if (field !== undefined && (field.isEmpty() || field.figure.colorType !== figure.colorType)) {
                possibleFields.push(field);
            }
        }

        var x = field.x;
        var y = field.y;

        var fields = handle.fields;

        /**
         * first detect all possible fields, where the king can move to and push them into the given moves array
         */
        checkField(x-1, y,   fields, moves);
        checkField(x-1, y+1, fields, moves);
        checkField(x,   y+1, fields, moves);
        checkField(x+1, y+1, fields, moves);
        checkField(x+1, y,   fields, moves);
        checkField(x+1, y-1, fields, moves);
        checkField(x,   y-1, fields, moves);
        checkField(x-1, y-1, fields, moves);
    }

    /**
     * Each mod must prevent the same signature of methods. So ensure to have
     * the same structure like an interface for the logic module.
     */   
    var mod = {

        possibleMoves: function(handle, figure, field, moves, logic, ignoreFinalMovesCheckup) {

            function setKingToSimulatedField(simulatedField) {
                if (simulatedField.isOccupied()) {
                    throw "KingMod::possibleMoves - Simulation error! Field is occupied x: " +
                        simulatedField.x +
                        ", y: " +
                        simulatedField.y;
                }

                // will return a may hit figure. Is needed to restore the origin state
                return board.moveFigure(field, simulatedField, true);
            }

            function setKingToOriginField(simulatedField, mayHitFigure) {
                board.moveFigure(simulatedField, field, true);

                // set the may hit figure back to the origin field
                simulatedField.figure = mayHitFigure;
            }

            /**
             * perform this part after the possibleMoves checkup!
             *
             * Castling is permissible if and only if all of the following conditions hold (Schiller 2001:19):
             *
             * 1. The king and the chosen rook are on the player's first rank.
             * 2. Neither the king nor the chosen rook has previously moved.
             * 3. There are no pieces between the king and the chosen rook.
             * 4. The king is not currently in check.
             * 5. The king does not pass through a square that is attacked by an enemy piece.
             * 6. The king does not end up in check. (True of any legal move.)
             *
             */
            function checkCastling() {

                function isCastlingChecked(x, y) {
                    var possibleField = common.getFieldSafely(fields, x, y);
                    var mayHitFigure = setKingToSimulatedField(possibleField);

                    var isChecked = logic.isChecked(handle, possibleField);

                    setKingToOriginField(possibleField, mayHitFigure);
                    console.log('isCastlingChecked', x, y, isChecked);
                    return isChecked;
                }

                var fields = handle.fields;

                /**
                 * think of the special long/short castling
                 */
                if (figure.notMoved) {

                    var x = field.x;
                    var y = field.y;

                    /**
                     * differ between black and white side
                     */
                    switch (figure.colorType) {
                        case finals.colorTypes.WHITE:
                            y = 1;
                            break;
                        case finals.colorTypes.BLACK:
                            y = 8;
                            break;
                    }

                    var xShortRook = 8;
                    var xShortDestField = 7;
                    var xShortFieldToCheck = 6;

                    var xLongRook = 1;
                    var xLongDestField = 3;
                    var xLongFieldToCheckOne = 2;
                    var xLongFieldToCheckTwo = 4;

                    /**
                     * check the short castling
                     */
                    var shortCastlingRook = common.getFieldSafely(fields, xShortRook, y).figure;
                    var shortCastlingField = common.getFieldSafely(fields, xShortDestField, y);
                    if (shortCastlingRook !== undefined &&
                        shortCastlingRook.notMoved &&
                        common.getFieldSafely(fields, xShortFieldToCheck, y).isEmpty() &&
                        shortCastlingField.isEmpty() &&
                        !logic.isChecked(handle, field) &&
                        !isCastlingChecked(xShortFieldToCheck, y) &&
                        !isCastlingChecked(xShortDestField, y)) {
                        moves.push(shortCastlingField);
                    }

                    /**
                     * check the long castling
                     */
                    var longCastlingRook = common.getFieldSafely(fields, xLongRook, y).figure;
                    var longCastlingField = common.getFieldSafely(fields, xLongDestField, y);
                    if (longCastlingRook !== undefined &&
                        longCastlingRook.notMoved &&
                        common.getFieldSafely(fields, xLongFieldToCheckOne, y).isEmpty() &&
                        common.getFieldSafely(fields, xLongFieldToCheckTwo, y).isEmpty() &&
                        longCastlingField.isEmpty() &&
                        !logic.isChecked(handle, field) &&
                        !isCastlingChecked(xLongFieldToCheckOne, y) &&
                        !isCastlingChecked(xLongFieldToCheckTwo, y) &&
                        !isCastlingChecked(xLongDestField, y)) {
                        moves.push(longCastlingField);
                    }

                }
            }

            /**
             * determine all relevant moves
             */
            checkBasicPossibleMoves(handle, figure, field, moves, logic);

            // TODO following block obsolete, if finalPossibleMovesCheckup works at this place!
            //var basicPossibleFields = checkBasicPossibleMoves(handle, figure, field, moves, logic);
            //_.each(basicPossibleFields, function(possibleField) {
            //
            //    /**
            //     * it must be simulated, if the possibleField can be checked by any enemy figure. For that this king
            //     * has to move to that field, so that finally some logic function can be reused to find out if the king
            //     * is checked or not.
            //     */
            //    var mayHitFigure = setKingToSimulatedField(possibleField);
            //
            //    if (!logic.isChecked(handle, possibleField)) {
            //        moves.push(possibleField);
            //    }
            //
            //    setKingToOriginField(possibleField, mayHitFigure);
            //
            //});
            common.finalPossibleMovesCheckup(handle, figure, field, moves, logic, ignoreFinalMovesCheckup);

            checkCastling();

        },

        canCheck: function(handle, figure, field, kingsField, logic, ignoreFinalMovesCheckup) {
            var possibleFields = [];
            checkBasicPossibleMoves(handle, figure, field, possibleFields, logic, ignoreFinalMovesCheckup);
            return common.containsField(kingsField, possibleFields);
        },

        performMoveTo: function(handle, sourceField, destField, moveToResult) {

            function moveRook(fromX, toX, y) {
                var fromField = common.getFieldSafely(handle.fields, fromX, y);
                var toField = common.getFieldSafely(handle.fields, toX, y);

                if (toField.isOccupied()) {
                    throw 'KingMod::performMoveTo::moveRook - Something went wrong. The toField is already occupied. Castling impossible.';
                }

                board.moveFigure(fromField, toField);
            }

            common.performMoveTo(handle, sourceField, destField, moveToResult);

            /**
             * check, if castling is performed. During castling it is the only move, where the king will move two fields
             * instead of only one field. So simply compare the x coordinates and determine king-side or queen-side
             * castling
             */
            var sourceX = sourceField.x;
            var destX = destField.x;
            var y = sourceField.y;

            var distanceCheckUp = Math.abs(sourceX - destX) > 1;
            if (distanceCheckUp && sourceX < destX) {
                // king-side castling
                moveToResult.castlingType = finals.pgnTypes.KINGSIDE_CASTLING;
                moveRook(8, 6, y);
            } else if (distanceCheckUp && sourceX > destX) {
                // queen-side castling
                moveToResult.castlingType = finals.pgnTypes.QUEENSIDE_CASTLING;
                moveRook(1, 4, y);
            }
        }

    };
   
    return mod;   
    
    
};


