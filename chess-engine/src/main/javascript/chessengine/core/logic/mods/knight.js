/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createKnightMod(common, finals) {
   
    /**
     * Each mod must prevent the same signature of methods. So ensure to have
     * the same structure like an interface for the logic module.
     */   
    var mod = {

        possibleMoves: function(handle, figure, field, moves, logic, ignoreFinalMovesCheckup) {

            var x = field.x;
            var y = field.y;

            var fields = handle.fields;

            var fieldsToCheck = [
                common.getFieldSafely(fields, x-2, y+1),
                common.getFieldSafely(fields, x-1, y+2),
                common.getFieldSafely(fields, x+1, y+2),
                common.getFieldSafely(fields, x+2, y+1),
                common.getFieldSafely(fields, x+2, y-1),
                common.getFieldSafely(fields, x+1, y-2),
                common.getFieldSafely(fields, x-1, y-2),
                common.getFieldSafely(fields, x-2, y-1)
            ];

            _.each(fieldsToCheck, function(fieldToCheck) {
                if (fieldToCheck !== undefined && (fieldToCheck.isEmpty() || fieldToCheck.figure.colorType !== figure.colorType)) {
                    moves.push(fieldToCheck);
                }
            });


            common.finalPossibleMovesCheckup(handle, figure, field, moves, logic, ignoreFinalMovesCheckup);
        },

        canCheck: function(handle, figure, field, kingsField, logic, ignoreFinalMovesCheckup) {
            var possibleFields = [];
            this.possibleMoves(handle, figure, field, possibleFields, logic, ignoreFinalMovesCheckup);
            return common.containsField(kingsField, possibleFields);
        },

        performMoveTo: function(handle, sourceField, destField, moveToResult) {
            common.performMoveTo(handle, sourceField, destField, moveToResult);
        }

    };
   
    return mod;   
    
    
};


