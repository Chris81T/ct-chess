/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createPawnMod(common, epgn, finals) {
   
    /**
     * Each mod must prevent the same signature of methods. So ensure to have
     * the same structure like an interface for the logic module.
     */   
    var mod = {

        possibleMoves: function(handle, figure, field, moves, logic, ignoreFinalMovesCheckup) {

            /**
             * instead of the common logic mod here "forward" is mentioned in the scope of this pawn (colorType)
             */
            function checkForwardLeftOrRightField(fieldToCheck) {
                if (fieldToCheck !== undefined &&
                    fieldToCheck.isOccupied() &&
                    fieldToCheck.figure.colorType !== figure.colorType) {
                    moves.push(fieldToCheck);
                }
            }

            var x = field.x;
            var y = field.y;
            
            var fields = handle.fields;

            /**
             * influences the direction of this pawn according to the colorType
             * @type {number}
             */
            var yMul = 0;
            
            /**
             * is needed for a checkup. Only if this pawn is set to folloing 
             * defined value (check the switch), an en passant checkup is 
             * relevant.
             */
            var yEnPassant = 0;
                
            switch (figure.colorType) {
                case finals.colorTypes.WHITE:
                    yMul = 1;
                    yEnPassant = 5;
                    break;
                case finals.colorTypes.BLACK:
                    yMul = -1;
                    yEnPassant = 4;
                    break;
            }

            var fieldForward = common.getFieldSafely(fields, x, y+1 * yMul);
            var twoFieldsForward = common.getFieldSafely(fields, x, y+2 * yMul);

            // first check, if one field forward is free
            if (fieldForward.isEmpty()) {
                moves.push(fieldForward);

                // if a pawn is not moved, the pawn can possibly move two field forward
                if (figure.notMoved && twoFieldsForward.isEmpty()) {
                    moves.push(twoFieldsForward);
                }

            }

            // possibly the figure can attack some enemy figure
            var fieldForwardLeft = common.getFieldSafely(fields, x-1, y+1 * yMul);
            var fieldForwardRight = common.getFieldSafely(fields, x+1, y+1 * yMul);

            checkForwardLeftOrRightField(fieldForwardLeft);
            checkForwardLeftOrRightField(fieldForwardRight);
              
            /**
             * check en passant. If y is the same, check the last moved enemy figure!
             */
            var lastMoveToResult = epgn.getLastMoveToResult(handle);

            /**
             * fromField.y === yEnPassant + yMul * 2 -->
             * this will check, if the last moved enemy figure has moved two fields forward. The from coordinate is known.
             * So simply equal it with y = 2 or 7 according to the color of the figure
             */
            if (y === yEnPassant &&
                lastMoveToResult !== undefined &&
                lastMoveToResult.movedFigure.colorType !== figure.colorType &&
                lastMoveToResult.movedFigure.figureType === finals.figureTypes.PAWN &&
                lastMoveToResult.fromField.y === yEnPassant + yMul * 2 && (
                    lastMoveToResult.toField.x + 1 === x ||
                    lastMoveToResult.toField.x - 1 === x)) {
                var enPassantResultField = common.getFieldSafely(fields, lastMoveToResult.toField.x, y+1 * yMul);

                console.log('app/logic/mods/pawn - this pawn = ', figure, ' is able to play en passant to hit enemy pawn = ',
                    lastMoveToResult.movedFigure);

                moves.push(enPassantResultField);
            }

            common.finalPossibleMovesCheckup(handle, figure, field, moves, logic, ignoreFinalMovesCheckup);
        },

        canCheck: function(handle, figure, field, kingsField, logic, ignoreFinalMovesCheckup) {
            var possibleFields = [];
            this.possibleMoves(handle, figure, field, possibleFields, logic, ignoreFinalMovesCheckup);
            return common.containsField(kingsField, possibleFields);
        },

        performMoveTo: function(handle, sourceField, destField, moveToResult) {

            // before performing the common move to, check, if some figure exists at the destField
            var originDestFieldFigure = destField.figure;

            common.performMoveTo(handle, sourceField, destField, moveToResult);

            // now the pawn is moved to the destField
            var movedPawn = destField.figure;
            var yPawnTransform = 0;
            var yOffset = 0;

            switch (movedPawn.colorType) {
                case finals.colorTypes.WHITE:
                    yPawnTransform = 8;
                    yOffset = 1;
                    break;
                case finals.colorTypes.BLACK:
                    yPawnTransform = 1;
                    yOffset = -1;
                    break;
            }

            /**
             * check pawn transformation
             */
            if (destField.y === yPawnTransform) {
                moveToResult.resultType = finals.moveToResult.DECISION_NEEDED;
                moveToResult.pawnTransformation.possible = true;
                moveToResult.pawnTransformation.pawnFigure = movedPawn;
            }

            /**
             * check may performed en passant. The x coordinate from a pawn is only different, if the pawn hits an
             * enemy figure. If to that situation also the destination field is empty, it must be en-passant. So finally
             * set the hitFigure and remove the hit enemy pawn from the field beside
             */
            if (originDestFieldFigure === undefined &&
                sourceField.x !== destField.x) {
                var enemyPawnField = common.getFieldSafely(handle.fields, destField.x, destField.y - yOffset);
                var enemyPawn = enemyPawnField.figure;
                if (enemyPawn !== undefined &&
                    enemyPawn.figureType === finals.figureTypes.PAWN &&
                    enemyPawn.colorType !== movedPawn.colorType) {
                    enemyPawnField.figure = undefined;
                    moveToResult.hitFigure = enemyPawn;
                    moveToResult.hitFigureField = enemyPawnField;
                } else {
                    throw 'PawnMod::performMoveTo - En-passant procedure failed. Found enemyPawn = ' + enemyPawn;
                }
            }

        }

    };
   
    return mod;   
    
    
};


