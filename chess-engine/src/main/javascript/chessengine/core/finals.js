/**
 * Created by christian on 17.12.2015.
 *
 * This is a plain written core to provide playing chess (human centered)
 *
 * The goal is to have some logic, that will run without angular. So if the internal core is working, the ui part can
 * be swapped with some cool other stuff.
 *
 */

function createFinals() {

    var finals = {

        fieldTypes: {
            BORDER:     'BORDER',
            WHITE:      'WHITE',
            BLACK:      'BLACK'
        },

        colorTypes: {
            WHITE:      'WHITE',
            BLACK:      'BLACK'
        },

        figureTypes: {
            KING:       'KING',
            QUEEN:      'QUEEN',
            ROOK:       'ROOK',
            BISHOP:     'BISHOP',
            KNIGHT:     'KNIGHT',
            PAWN:       'PAWN'
        },

        moveToResult: {
            UNKNOWN:            'UNKNOWN',
            OK:                 'OK',
            FAILED:             'FAILED',
            DECISION_NEEDED:    'DECISION_NEEDED'
        },

        pgnTypes: {
            KINGSIDE_CASTLING:      'O-O',
            QUEENSIDE_CASTLING:     'O-O-O',
            CAPTURE_MOVE:           'x',
            CHECKING_MOVE:          '+',
            CHECKMATING_MOVE:       '#'
        },

        gameStateTypes: {
            NORMAL:     'NORMAL',
            CHECK:      'CHECK',
            CHECKMATE:  'CHECKMATE',
            DEADLOCK:   'DEADLOCK',
            DRAW:       'DRAW'
        },

        pgnFigureTypes: {
            KING:       'K',
            QUEEN:      'Q',
            ROOK:       'R',
            BISHOP:     'B',
            KNIGHT:     'N',
            PAWN:       ''
        }

    };

    return finals;
};
