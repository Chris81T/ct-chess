/**
 * Created by christian on 17.12.2015.
 *
 * This is a plain written core to provide playing chess (human centered)
 *
 * The goal is to have some logic, that will run without angular. So if the internal core is working, the ui part can
 * be swapped with some cool other stuff.
 *
 */
function createEngine(_, logic, board, common, epgn, finals) {

    console.log('app/engine - injected modules are ', logic, board);

    function setWhiteAsActivePlayer(handle) {
        handle.activePlayer = finals.colorTypes.WHITE;
    }

    function setBlackAsActivePlayer(handle) {
        handle.activePlayer = finals.colorTypes.BLACK;
    }

    function switchActivePlayer(handle) {
        if (handle.activePlayer === finals.colorTypes.WHITE) {
            setBlackAsActivePlayer(handle);
        } else {
            setWhiteAsActivePlayer(handle);
        }
    }

    /**
     * 
     * @param {type} coord is a String with content like 'b5'
     * @returns {undefined|createEngine.extractCoord.engineAnonym$0}
     */
    function extractCoord(coord) {
        var splitted = coord.split('');

        if (splitted.length > 2) return undefined;

        return {
            bX: splitted[0],
            bY: splitted[1]
        };
    }

    /**
     * Is used by the public access (given from coordinate must first be extracted)
     * and internally.
     * 
     * @param {type} handle
     * @param {type} fromCoord
     * @returns {Array|undefined} an array of all fields, where the figure 
     *          behind the given coordinates can move to.
     */
    function parsePossibleMoves(handle, fromCoord) {
        var moves = logic.possibleMoves(handle, board.getField(handle, fromCoord.bX, fromCoord.bY));
        console.log('app/engine - possible determined moves = ', moves);
        return moves;
    }

    var engine = {

        version: '0.0.1',

        newGame: function() {

            function prepareHandle(handle) {
                setWhiteAsActivePlayer(handle);
            }

            var handle = {};

            logic.newGame(handle);
            board.newGame(handle);
            epgn.newGame(handle);

            prepareHandle(handle);

            console.log('app/engine - about to return new created handle = ', handle);
            return handle;
        },
        
        /**
         * 
         * @param {type} savedGame
         * @returns {undefined}
         */
        loadGame: function(savedGame) {
           throw 'NOT YET IMPLEMENTED!'; 
        },
        
        /**
         * 
         * @param {type} handle
         * @returns {String} the saved game in form of a string according to given
         *          handle
         */
        saveGame: function(handle) {
           throw 'NOT YET IMPLEMENTED!';             
        },

        /**
         * useful for the client to get relevant information to paint the fields for instance.
         */
        parseFields: function(handle, clientFx) {
           board.performOnFields(handle, clientFx);
        },

        /**
         *
         * @param handle knows all relevant information about the current game
         *
         * allowed values:
         * bX board X --> a, b, c, d, e, f, g, h
         * bY board Y --> 1, 2, 3, 4, 5, 6, 7, 8
         *
         * @param from(bXbY) String
         * @param to(bXbY) String
         * @return a result object with more details (is movement possible or not, is a pawn transformation possible,
         *         etc..., contains the move notation)
         */
        moveTo: function(handle, from, to) {

            function generateFailedResult() {
                var result = common.createMoveToResult();
                result.resultType = finals.moveToResult.FAILED;
                return result;
            }

            var fromCoord = extractCoord(from);
            var toCoord = extractCoord(to);

            if (fromCoord === undefined || toCoord === undefined) {
                console.log('app/engine - Invalid coordinates! From = ', from, ' to = ', to);
                return generateFailedResult();
            }

            var sourceField = board.getField(handle, fromCoord.bX, fromCoord.bY);
            var sourceFigure = sourceField.figure;

            if (sourceFigure !== undefined && sourceFigure.colorType !== handle.activePlayer) {
                console.log('app/engine - Wrong player! Active player is = ', handle.activePlayer);
                return generateFailedResult();
            }

            var moves = parsePossibleMoves(handle, fromCoord);
            var destField = board.getField(handle, toCoord.bX, toCoord.bY);

            /**
             * check, if moves contains the destField. If yes, then the movement is valid and can be performed.
             */
            if (_.some(moves, function(move) { return common.theSame(move, destField); })) {
                console.log('app/engine - moveTo is valid. The handle will be adapted', moves, destField, handle);
                return logic.performMoveTo(handle, sourceField, destField);
            } else {
                console.log('app/engine - moveTo is invalid!', moves, destField);
                return generateFailedResult();
            }
        },

        /**
         * Normally after calling moveTo this function can be called to complete
         * the movement and record the pgn for that move. But during pawn 
         * transformation some user-decision is needed to complete the movement
         * and finally record the needed pgn.
         * 
         * @param {type} moveToResult
         * @returns {undefined}
         */
        completeMoveTo: function(handle, moveToResult) {

            function completeResult() {
                console.log('app/engine - completeMoveTo - completeResult');

                /**
                 * finally check, if a special game-state is entered after the performed move. If such a special state
                 * is identified, return it to the client.
                 *
                 * For that the finals holds a bunch of gameStates.
                 */
                var enemyColorType = moveToResult.colorType === finals.colorTypes.WHITE ? finals.colorTypes.BLACK : finals.colorTypes.WHITE;
                var gameState = logic.checkGameState(handle, enemyColorType);

                epgn.handleMoveToResult(handle, moveToResult, gameState);

                /**
                 * check, if the game is definitely over. If not, switch the active player
                 */
                if (gameState === finals.gameStateTypes.NORMAL || gameState === finals.gameStateTypes.CHECK) {
                    switchActivePlayer(handle);
                }

                return gameState;
            }

            function checkDecision() {
                console.log('app/engine - completeMoveTo - checkDecision');

                if (moveToResult.pawnTransformation.possible) {
                    var newFigureType = moveToResult.pawnTransformation.newFigureType;
                    moveToResult.pawnTransformation.newFigure = board.createNewFigureTo(newFigureType, moveToResult.colorType, moveToResult.toField);
                }

                return completeResult();
            }

            switch (moveToResult.resultType) {
                case finals.moveToResult.OK:
                    return completeResult();
                    break;
                case finals.moveToResult.DECISION_NEEDED:
                    return checkDecision();
                    break;
                case finals.moveToResult.FAILED:
                case finals.moveToResult.UNKNOWN:
                    throw 'Engine::completeMoveTo - Not allowed to call method with given resultType = ' + moveToResult.resultType;
                    break;
            }
        },

        /**
         * Is useful for the client to find out, where the figure is able to 
         * move to. Think about a preview / validation in the UI.
         * 
         * @param {type} handle
         * @param {type} from is a String with content like 'h6'
         * @returns {Array|undefined} an array of all fields, where the figure 
         *          behind the given coordinates can move to.
         */
        possibleMoves: function(handle, from) {
            return parsePossibleMoves(handle, extractCoord(from));
        }
        
    };

    return engine;
};
