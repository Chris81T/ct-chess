/**
 * Created by christian on 29.12.2015.
 *
 *  epgn --> "e"xtended "pgn"
 */

function createEpgn(_, finals) {

    function getFigureNotation(figureType) {
        switch (figureType) {
            case finals.figureTypes.BISHOP: return finals.pgnFigureTypes.BISHOP;
            case finals.figureTypes.KING:   return finals.pgnFigureTypes.KING;
            case finals.figureTypes.KNIGHT: return finals.pgnFigureTypes.KNIGHT;
            case finals.figureTypes.PAWN:   return finals.pgnFigureTypes.PAWN;
            case finals.figureTypes.QUEEN:  return finals.pgnFigureTypes.QUEEN;
            case finals.figureTypes.ROOK:   return finals.pgnFigureTypes.ROOK;
            default: return undefined;
        }
    }

    var epgn = {
        
        newGame: function(handle) {

            function enrichHandle() {
                
                /**
                 * @returns current date in format: "YYYY.MM.DD"
                 */
                function now() {
                    var now = new Date();
                    return now.toISOString().substring(0, 10).replace('-','.');
                }
                
                return {
                    meta: {
                        event: null,
                        site: null,
                        date: now(),
                        round: null,
                        white: null,
                        black: null,
                        result: null                        
                    },
                    /**
                     * array of json with structure:
                     * { white: {move: moveToResult VALUE, timestamp: VALUE},
                     *   black: {move: moveToResult VALUE, timestamp: VALUE},
                      *  notation: knows the resulting notation }
                     */
                    moves: []
                };
            }
                
            handle.pgn = enrichHandle();

        },
        
        setMeta: function(handle, event, site, round, white, black) {
            var meta = handle.pgn.meta;
            meta.event = event;
            meta.site = site;
            meta.round = round;
            meta.white = white;
            meta.black = black;
        },
        
        setResult: function(handle, result) {
            handle.pgn.meta.result = result;
        },
        
        /**
         * enrich the given result with the pg-notation and append it to the moves array
         *
         * PGN Hints:
         * ******** *
         *
         * [figureType]
         * [opt. movedFigure from coordinate (typically 'y' only. Special case is the knight) - if a pawn hits a figure, it is mandatory]
         * [x --> if hitFigure is defined]
         * [movedFigure to coordinate]
         * [+ --> if checked]
         * [# --> if checkmate]
         * [=Q --> if pawn transformation --> Q = queen --> figureType of transformed figure]
         *
         * @param {type} moveToResult knows all relevant details to create
         */
        handleMoveToResult: function(handle, moveToResult, gameState) {

            var moves = handle.pgn.moves;

            /**
             * will initially create the notation with an index and the white movement
             */
            function generateWhiteNotation() {
                console.log('app/epgn - generate pgn for moveToResult (white side) = ', moveToResult, ', gameState = ', gameState);
                var notation = (moves.length + 1) + '. ';

                // check if castling is performed
                var castling = moveToResult.castlingType;
                if (castling !== undefined) {
                    // castling knows the relevant notation. Simply append it
                    notation += castling;
                } else {
                    var hitFigure = moveToResult.hitFigure;
                    var movedFigureType = moveToResult.movedFigure.figureType;

                    // first the moved figure notation is needed
                    notation += getFigureNotation(movedFigureType);

                    if (movedFigureType === finals.figureTypes.PAWN &&
                        hitFigure === undefined) {

                    } else if (movedFigureType === finals.figureTypes.PAWN &&
                        hitFigure !== undefined) {

                    } else {
                        /**
                         * after a pawn transformation or normally a rook or knight more than one figures from same type
                         * exists. Possibly both figures (also it would be possible to have more the same figures - pawn
                         * transformation - so handle it as an array) could move to the same "to" field
                         */
                    }

                }

                return notation
            }

            /**
             * will append the existing notation
             */
            function generateBlackNotation() {
                console.log('app/epgn - generate pgn for moveToResult (black side) = ', moveToResult, ', gameState = ', gameState);

            }

            var entry = {
                move: moveToResult,
                timestamp: new Date()
            };

            if (moveToResult.colorType === finals.colorTypes.WHITE) {

                var move = {
                  white: entry,
                  black: undefined,
                  notation: generateWhiteNotation()
                };

                moves.push(move);

            } else {

                // get the last move to append the black move
                var move = _.last(moves);

                move.black = entry;

                // complete the notation for this epgn move entry
                move.notation += generateBlackNotation();

            }

            console.log('app/epgn - handleMoveToResult finished. Content of moves = ', moves);

        },

        getLastMoveToResult: function(handle) {
            var moves = handle.pgn.moves;
            if (moves.length === 0) return undefined;

            var lastMove = _.last(moves);
            var lastMoveToResult = lastMove.black === undefined ? lastMove.white.move : lastMove.black.move;

            return lastMoveToResult;
        },

        /**
         * EXPORT PGN
         * @param {type} handle
         * @returns a String with export of given pgn (known by the handle)
         */
        exportPgn: function(handle) {
            
            function genMetaValue(key, value) {
                return '[' + key + ' "' + value + '"]\n';
            }
            
            function genMoveEntry(index, move) {
                
            }
            
            var meta = handle.pgn.meta;
            var moves = handle.pgn.moves;
            
            var pgnAsString = '';
            
            pgnAsString += genMetaValue('Event', meta.event);
            pgnAsString += genMetaValue('Site', meta.site);
            pgnAsString += genMetaValue('Date', meta.date);
            pgnAsString += genMetaValue('Round', meta.round);
            pgnAsString += genMetaValue('White', meta.white);
            pgnAsString += genMetaValue('Black', meta.black);
            pgnAsString += genMetaValue('Result', meta.result);
            
            pgnAsString += '\n';
                        
            _.each(moves, function(move) {
                // TODO each moves knows the
            });
            
            return pgnAsString;
        },
        
        /**
         * IMPORT PGN
         * @param {type} handle
         * @param {String} pgnAsString will be used for the import. The result 
         *        will be set inside the handle -> pgn
         */
        importPgn: function(handle, pgnAsString) {
           throw 'NOT YET IMPLEMENTED!';                         
        }
        
    };

    return epgn;

};