/**
 * Created by christian on 17.12.2015.
 */

function createBoard(_, finals) {

    /**
     * is only needed as a helper id for bug fixing
     * @type Number
     */
    var counter = 0;

    function buildFigure(figureType, colorType) {
        return {
            figureType: figureType,
            colorType: colorType,
            notMoved: true,
            movedCounter: 0,
            id: counter++
        }
    }

    function bindFigureToField(field, figure) {
        field.figure = figure;
    }

    /**
     * B == Border
     * F == Field
     *
     *    Y
     *    9[B][B][B][B][B][B][B][B][B][B]
     * 8  8[B][F][F][F][F][F][F][F][F][B]
     * 7  7[B][F][F][F][F][F][F][F][F][B]
     * 6  6[B][F][F][F][F][F][F][F][F][B]
     * 5  5[B][F][F][F][F][F][F][F][F][B]
     * 4  4[B][F][F][F][F][F][F][F][F][B]
     * 3  3[B][F][F][F][F][F][F][F][F][B]
     * 2  2[B][F][F][F][F][F][F][F][F][B]
     * 1  1[B][F][F][F][F][F][F][F][F][B]
     *    0[B][B][B][B][B][B][B][B][B][B]
     *      0  1  2  3  4  5  6  7  8  9 X
     *
     *         a  b  c  d  e  f  g  h
     *
     *
     *    x|y x|y   x|y x|y
     *    0|0 0|1   1|0 1|1 ...
     *  [['A','B'],['C','D'],[],[],[],[],[],[],[],[]]
     *
     *
     */
    function buildFields(handle) {

        function buildField(x, y, fieldType) {
            return {
                x: x,
                y: y,
                fieldType: fieldType,
                figure: undefined,
                isBorder: function() { return this.fieldType === finals.fieldTypes.BORDER; },
                isEmpty: function() { return this.figure === undefined; },
                isOccupied: function() { return !this.isEmpty(); }
            }
        }

        function buildBorderRow(rowNumber) {
            var borderRow = [];

            for (var i=0; i < 10; i++) {
                borderRow.push(buildField(rowNumber, i, finals.fieldTypes.BORDER));
            }

            return borderRow;
        }

        function buildRow(rowNumber, firstFieldType) {
            var row = [];

            function getFieldType(colNumber) {
                if (colNumber === 0 || colNumber === 9) {
                    return finals.fieldTypes.BORDER;
                }
                var fieldType = firstFieldType;
                firstFieldType = firstFieldType === finals.fieldTypes.WHITE ? finals.fieldTypes.BLACK : finals.fieldTypes.WHITE;
                return fieldType;
            }

            for (var i=0; i < 10; i++) {
                row.push(buildField(rowNumber, i, getFieldType(i)));
            }

            return row;
        }

        var columns = [
            buildBorderRow(0),
            buildRow(1, finals.fieldTypes.BLACK),
            buildRow(2, finals.fieldTypes.WHITE),
            buildRow(3, finals.fieldTypes.BLACK),
            buildRow(4, finals.fieldTypes.WHITE),
            buildRow(5, finals.fieldTypes.BLACK),
            buildRow(6, finals.fieldTypes.WHITE),
            buildRow(7, finals.fieldTypes.BLACK),
            buildRow(8, finals.fieldTypes.WHITE),
            buildBorderRow(9)
        ];

        handle.fields = columns;
    }

    function buildFigures(handle) {

        // white figures
        bindFigureToField(board.getField(handle, 'a', '1'), buildFigure(finals.figureTypes.ROOK, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'b', '1'), buildFigure(finals.figureTypes.KNIGHT, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'c', '1'), buildFigure(finals.figureTypes.BISHOP, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'd', '1'), buildFigure(finals.figureTypes.QUEEN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'e', '1'), buildFigure(finals.figureTypes.KING, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'f', '1'), buildFigure(finals.figureTypes.BISHOP, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'g', '1'), buildFigure(finals.figureTypes.KNIGHT, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'h', '1'), buildFigure(finals.figureTypes.ROOK, finals.colorTypes.WHITE));

        bindFigureToField(board.getField(handle, 'a', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'b', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'c', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'd', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'e', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'f', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'g', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));
        bindFigureToField(board.getField(handle, 'h', '2'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.WHITE));

        // black figures
        bindFigureToField(board.getField(handle, 'a', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'b', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'c', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'd', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'e', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'f', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'g', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'h', '7'), buildFigure(finals.figureTypes.PAWN, finals.colorTypes.BLACK));

        bindFigureToField(board.getField(handle, 'a', '8'), buildFigure(finals.figureTypes.ROOK, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'b', '8'), buildFigure(finals.figureTypes.KNIGHT, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'c', '8'), buildFigure(finals.figureTypes.BISHOP, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'd', '8'), buildFigure(finals.figureTypes.QUEEN, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'e', '8'), buildFigure(finals.figureTypes.KING, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'f', '8'), buildFigure(finals.figureTypes.BISHOP, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'g', '8'), buildFigure(finals.figureTypes.KNIGHT, finals.colorTypes.BLACK));
        bindFigureToField(board.getField(handle, 'h', '8'), buildFigure(finals.figureTypes.ROOK, finals.colorTypes.BLACK));

    }

    var board = {

        /**
         * has to create the fields for given handle for instance
         */
        newGame: function(handle) {

            buildFields(handle);
            buildFigures(handle);

        },

        /**
         * Simply returns requested field or undefined, if coordinates are wrong
         * @param bX board X --> a, b, c, d, e, f, g, h
         * @param bY board > --> 1, 2, 3, 4, 5, 6, 7, 8
         */
        getField: function(handle, bX, bY) {

            function getX() {
                switch (bX) {
                    case 'a' :
                    case 'A' :
                        return 1;
                    case 'b' :
                    case 'B' :
                        return 2;
                    case 'c' :
                    case 'C' :
                        return 3;
                    case 'd' :
                    case 'D' :
                        return 4;
                    case 'e' :
                    case 'E' :
                        return 5;
                    case 'f' :
                    case 'F' :
                        return 6;
                    case 'g' :
                    case 'G' :
                        return 7;
                    case 'h' :
                    case 'H' :
                        return 8;
                }
                return -1;
            }

            function getY() {
                var yNr = Number(bY);
                return _.isNumber(yNr) ? yNr : -1;
            }

            function checkCoord(coord) { return coord > 0 && coord < 9; }

            var x = getX();
            var y = getY();

            if (checkCoord(x) && checkCoord(y)) {
                return handle.fields[x][y];
            }

            return undefined;
        },

        /**
         * useful for the client to get relevant information to paint the fields for instance.
         */
        performOnFields: function(handle, clientFx) {
            _.each(handle.fields, function(column) {
                _.each(column, function(field) {
                    if (field.fieldType !== finals.fieldTypes.BORDER) clientFx(field);
                });
            });
        },

        /**
         * creates an array of fields which are occupied by figures with given colorType
         * @param handle
         * @param colorType
         * @returns {Array}
         */
        getOccupiedFields: function(handle, colorType) {
            var occupiedFields = [];

            this.performOnFields(handle, function(field) {
                if (field.isOccupied() && field.figure.colorType === colorType) {
                    occupiedFields.push(field);
                }
            });

            return occupiedFields;
        },

        /**
         * creates an array of fields which are occupied by enemy figures according to given colorType
         * @param handle
         * @param colorType
         * @returns {Array}
         */
        getEnemyOccupiedFields: function(handle, colorType) {
            var occupiedFields = [];

            this.performOnFields(handle, function(field) {
                if (field.isOccupied() && field.figure.colorType !== colorType) {
                    occupiedFields.push(field);
                }
            });

            return occupiedFields;
        },

        /**
         * this method will identify the king to given colorType
         * @param handle
         * @param colorType
         */
        getKingsField: function(handle, colorType) {
            var fields = handle.fields;

            for (var i=1, length=fields.length-1; i<length; i++) {
                for (var j=1, length=fields.length-1; j<length; j++) {
                    var field = fields[i][j];
                    var figure = field.figure;
                    if (figure !== undefined &&
                        figure.colorType === colorType &&
                        figure.figureType === finals.figureTypes.KING) {
                        return field;
                    }
                }
            }

            // should normally never happen
            throw 'Board::getKingsField - no king found for colorType = ' + colorType;
        },

        /**
         *
         * @param fromField
         * @param toField
         * @param simulatedMove
         * @returns the maybe hit figure
         */
        moveFigure: function(fromField, toField, simulatedMove) {
            var mayHitFigure = toField.figure;
            toField.figure = fromField.figure;
            fromField.figure = undefined;
            
            /**
             * sometimes only a simulation will be performed (check the king mod).
             * Therefor it is relevant to ignore the counter increasement. 
             */
            if (!simulatedMove) {
                toField.figure.movedCounter++;
                toField.figure.notMoved = false;
            }

            return mayHitFigure;
        },

        /**
         * Will create a new figure and place it to given field
         * @param newFigureType
         * @param field
         * @returns the new created figure
         */
        createNewFigureTo: function(figureType, colorType, field) {
            var newFigure = buildFigure(figureType, colorType);
            bindFigureToField(field, newFigure);
            return newFigure;
        }

    };

    return board;

};