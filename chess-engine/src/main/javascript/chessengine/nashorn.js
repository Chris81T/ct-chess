/**
 * workaround using inside the code the typical console.log method. Nashhorn
 * provides for that a print method.
 */
var console = {
    log: print
}

load("classpath:chessengine/lib/underscore.js");

load("classpath:chessengine/core/logic/mods/bishop.js");
load("classpath:chessengine/core/logic/mods/common.js");
load("classpath:chessengine/core/logic/mods/king.js");
load("classpath:chessengine/core/logic/mods/knight.js");
load("classpath:chessengine/core/logic/mods/pawn.js");
load("classpath:chessengine/core/logic/mods/queen.js");
load("classpath:chessengine/core/logic/mods/rook.js");

load("classpath:chessengine/core/logic/main.js");

load("classpath:chessengine/core/board.js");
load("classpath:chessengine/core/engine.js");
load("classpath:chessengine/core/epgn.js");
load("classpath:chessengine/core/finals.js");

function createChessEngine() {

    var finals = createFinals();

    var board = createBoard(_, finals);
    var epgn = createEpgn(_, finals);
    
    var commonMod = createCommonMod(_, board, finals);
        
    var bishopMod = createBishopMod(commonMod, finals);
    var kingMod = createKingMod(commonMod, finals, board);
    var knightMod = createKnightMod(commonMod, finals);
    var pawnMod = createPawnMod(commonMod, epgn, finals);
    var queenMod = createQueenMod(commonMod, finals);
    var rookMod = createRookMod(commonMod, finals);

    var logic = createLogic(_, finals, board, kingMod, queenMod, rookMod, 
        bishopMod, knightMod, pawnMod, commonMod);    
    
    var engine = createEngine(_, logic, board, commonMod, epgn, finals);
    
    return engine;
    
}

var engine = createChessEngine();

function newGame() {
    print('nashorn - chess-engine: start new game');
    return engine.newGame();    
}

function loadGame(savedGame) {
    print('nashorn - chess-engine: load game');
    return engine.loadGame();    
}

function saveGame(handle) {
    print('nashorn - chess-engine: save game');
    return engine.saveGame();    
}

function moveTo(handle, from, to) {
    print('nashorn - chess-engine: move ', from, ' to ', to);
    return engine.moveTo(handle, from, to);    
}

function completeMoveTo(handle, moveToResult) {
    print('nashorn - chess-engine: complete movement with given result = ', moveToResult);
    return engine.completeMoveTo(handle, moveToResult);    
}

function possibleMoves(handle, from) {
    print('nashorn - chess-engine: determine possible moves for figure from ', from);
    return engine.possibleMoves(handle, from);  
}

