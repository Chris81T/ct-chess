/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.helpers.objects;

import de.ct.chess.helpers.UnaryIntGenericsOperator;
import java.util.Objects;

/**
 * 
 * @author christian
 */
public class FieldCoord {
    
    private final int numX;
    private final int numY;
    
    private final String x;
    private final String y;
    
    /**
     * @param numX 0 - 7
     * @param numY 0 - 7
     */
    public FieldCoord(int numX, int numY) {
        this.numX = numX;
        this.numY = numY;
        
        this.x = translateNumX(numX);
        this.y = translateNumY(numY);
    }

    public FieldCoord(String xy) {
        this(String.valueOf(xy.charAt(0)), String.valueOf(xy.charAt(1)));
    }
    
    public FieldCoord(String x, String y) {
        this.x = x.toLowerCase();
        this.y = y.toLowerCase();
        
        this.numX = translateX(this.x);
        this.numY = translateY(this.y);
    }
    
    /**
     * @return 0 - 7
     */
    public int getNumX() {
        return numX;
    }

    /**
     * @return 0 - 7
     */
    public int getNumY() {
        return numY;
    }
    
    /**
     * @return "a" - "h"
     */
    public String getX() {
        return x;
    }

    /**
     * @return "1" - "8"
     */
    public String getY() {
        return y;
    }

    /**
     * @return "a1" - "h8"
     */
    public String getCoord() {
        StringBuilder coord = new StringBuilder();
        coord.append(getX());
        coord.append(getY());
        return coord.toString();
    }
    
    public static String translateNumX(int numX) {
        switch(numX) {
            case 0: return "a";
            case 1: return "b";
            case 2: return "c";
            case 3: return "d";
            case 4: return "e";
            case 5: return "f";
            case 6: return "g";
            case 7: return "h";
            default: return null;
        }
    }
    
    private static <T> T checkNumY(int numY, UnaryIntGenericsOperator<T> function) {
        if (numY < 0 || numY > 7) throw new IllegalStateException();
        return function.apply(numY);
    }
    
    public static String translateNumY(int numY) {
        try {
            return checkNumY(numY, y -> String.valueOf(y + 1));                    
        } catch (IllegalStateException e) {
            return null;
        }
    }

    public static int translateX(String x) {
        switch(x) {
            case "a": return 0;
            case "b": return 1;
            case "c": return 2;
            case "d": return 3;
            case "e": return 4;
            case "f": return 5;
            case "g": return 6;
            case "h": return 7;
            default: return -1;
        }        
    }
    
    public static int translateY(String y) {
        try {
            int numY = Integer.valueOf(y) - 1;
            return checkNumY(numY, transY -> transY);
        } catch (NumberFormatException | IllegalStateException e) {
            return -1;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.numX;
        hash = 97 * hash + this.numY;
        hash = 97 * hash + Objects.hashCode(this.x);
        hash = 97 * hash + Objects.hashCode(this.y);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FieldCoord other = (FieldCoord) obj;
        if (this.numX != other.numX) {
            return false;
        }
        if (this.numY != other.numY) {
            return false;
        }
        if (!Objects.equals(this.x, other.x)) {
            return false;
        }
        if (!Objects.equals(this.y, other.y)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "FieldCoord{" + "numX=" + numX + ", numY=" + numY + ", x=" + x + 
                ", y=" + y + ", coord=" + getCoord() + ")";
    }
    
}
