/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.helpers.enums;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author christian
 */
public enum PgnType {
    
    KINGSIDE_CASTLING("O-O"),
    QUEENSIDE_CASTLING("O-O-O"),
    CAPTURE_MOVE("x"),
    CHECKING_MOVE("+"),
    CHECKMATING_MOVE("#");

    private final String value;

    private static final List<PgnType> enumTypes = Arrays.asList(PgnType.values());
    
    PgnType(String value) {
        this.value = value;
    }
    
    public static PgnType getEnum(String value) {       
        return enumTypes.stream()
            .filter(enumValue -> enumValue.getValue().equals(value))
            .findFirst()
            .orElseGet(null);
    }
    
    public String getValue() {
        return value;
    }
    
}
