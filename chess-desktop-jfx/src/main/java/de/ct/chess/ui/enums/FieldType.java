/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.ui.enums;

/**
 *
 * @author christian
 */
public enum FieldType {
    BLACK("black-field"), 
    WHITE("white-field");
    
    private final String cssClass;

    private FieldType(String cssClass) {
        this.cssClass = cssClass;
    }
    
    public String getCssClass() {
        return cssClass;
    }
    
}
