/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.ui;

import de.ct.chess.helpers.objects.FieldCoord;
import de.ct.chess.helpers.enums.Color;
import de.ct.chess.ui.enums.FieldType;
import de.ct.chess.helpers.enums.FigureType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 *
 * @author christian
 */
public class Chessboard extends Group {
    
    private static final int FIELD_XY_COUNT = 8;
    private static final int BOARD_PADDING_PX = 50;
    private static final int BOARD_MARGIN_PX = 80;
    private static final int FIELD_SIZE_PX = 100;
  
    private static final int MAX_BOARD_SIZE = FIELD_XY_COUNT * FIELD_SIZE_PX + 2 * BOARD_PADDING_PX +  2 * BOARD_MARGIN_PX;
    private static final int OFFSET = BOARD_MARGIN_PX + BOARD_PADDING_PX;
    
    private static final String BOARD_STYLE = "chessboard";
    private static final String FIELD_LABEL_STYLE = "field-label";
    
    private static final double TRANS_MOVE_SCALE_FACTOR = 1.25;
    private static final double TRANS_MOVE_AVG_DURATION = 600;

    private List<Field> fields = new ArrayList<>();
    private List<Figure> figures = new ArrayList<>();
    
    public Chessboard() {
        
        getStyleClass().add("chessboard");
        
        drawBoard();
        
    }
    
    public int getMaxBoardSize() {
        return MAX_BOARD_SIZE;
    }

    public List<Figure> getFigures() {
        return figures;
    }

    public List<Field> getFields() {
        return fields;
    }
    
    public Field getField(FieldCoord coord) {
        return fields.stream()
            .filter(field -> field.getFieldCoord().equals(coord))
            .findFirst()
            .orElseGet(null);
    }
    
    private void addFigure(Figure figure) {
        figures.add(figure);
        addNode(figure);
    }
    
    private void addField(Field field) {
        fields.add(field);
        addNode(field);
    }
    
    private void addNode(Node node) {
        getChildren().add(node);        
    }
    
    private void createField(int x, int y, double drawX, double drawY, FieldType fieldType) {
        Field field = new Field(x, y, drawX, drawY, FIELD_SIZE_PX, fieldType);        
        addField(field);
    }
    
    private void createFieldLabel(double drawX, double drawY, double wrappingWidth, String labelValue) {
        Text text = new Text(drawX, drawY, labelValue);        
        text.setWrappingWidth(wrappingWidth);
        text.getStyleClass().add(FIELD_LABEL_STYLE);
        addNode(text);
    }
    
    private double calcDrawX(int x) {
        return OFFSET + x * FIELD_SIZE_PX;
    }
    
    private double calcDrawY(int y) {
        return BOARD_MARGIN_PX - BOARD_PADDING_PX + (FIELD_XY_COUNT - y) * FIELD_SIZE_PX;
    }
    
    private void drawBoard() {

        final int boardSize = MAX_BOARD_SIZE - 2 * BOARD_MARGIN_PX;
        
        Rectangle board = new Rectangle(BOARD_MARGIN_PX, BOARD_MARGIN_PX, boardSize, boardSize);                       
        
        board.getStyleClass().add(BOARD_STYLE);
        addNode(board);
        
        IntStream.range(0, FIELD_XY_COUNT).forEach(y -> {
            IntStream.range(0, FIELD_XY_COUNT).forEach(x -> {
                
                final double drawX = calcDrawX(x);
                final double drawY = calcDrawY(y);
                
                createField(x, y, drawX, drawY, (x + y) % 2 == 0 ? FieldType.BLACK : FieldType.WHITE);  
                
                // TODO: correct center position of labels! Include the font size for calculation
                
                // draw: a b c d e f g h
                if (y == 0) {
                    final String xCoord = FieldCoord.translateNumX(x);
                    createFieldLabel(drawX,
                            drawY + FIELD_SIZE_PX + BOARD_PADDING_PX / 2, 
                            FIELD_SIZE_PX,
                            xCoord);
                }
                
                // draw: 1 2 3 4 5 6 7 8 
                if (x == 0) {
                    final String yCoord = FieldCoord.translateNumY(y);
                    createFieldLabel(OFFSET - BOARD_PADDING_PX, 
                            drawY + FIELD_SIZE_PX / 2, 
                            BOARD_PADDING_PX,
                            yCoord);
                }
                
            });
        });
        
    }

    void setScale(double newScale) {
        setScaleX(newScale);
        setScaleY(newScale);
    }
    
    public void placeFigure(FigureType figureType, Color color, FieldCoord coord) {
        Figure figure = new Figure(figureType, color, FIELD_SIZE_PX);
        
        Field field = getField(coord);
        field.bindFigure(figure);
        
        final int x = coord.getNumX();
        final int y = coord.getNumY();
        
        figure.setX(calcDrawX(x));
        figure.setY(calcDrawY(y));
        
        addFigure(figure);
    }
    
    private double getFieldDeltaX(Field from, Field to) {
        return to.getDrawX() - from.getDrawX();
    }
    
    private double getFieldDeltaY(Field from, Field to) {
        return to.getDrawY() - from.getDrawY();
    }
    
    public double getAvgDuration(FieldCoord from, FieldCoord to) {
        return getAvgDuration(getField(from), getField(to));
    }
    
    public double getAvgDuration(Field from, Field to) {
        double avgDuration = TRANS_MOVE_AVG_DURATION;
        
        /**
         * If the movement goes approximatly over more than four fields, the
         * duration time will be increased. Else it is to fast
         */
        if (Math.max(getFieldDeltaX(from, to), getFieldDeltaY(from, to)) / FIELD_SIZE_PX > 4) {
            avgDuration += avgDuration / 2;                     
        }
        
        return avgDuration;
    }

    public void moveFigure(FieldCoord fromCoord, FieldCoord toCoord, 
            FieldCoord hitFigureCoord, boolean silent) {
        moveFigure(fromCoord, toCoord, hitFigureCoord, silent, 0.0);
    }
    
    /**
     * Move a figure from 
     * @param fromCoord source field of figure
     * @param toCoord destination field of figure
     * @param hitFigureCoord remember en passant. In that case the hit figure is 
     *        not at the same field as the to field.
     * @param silent if true, it prevents performing an animation for movement
     * @param delay is relevant for the castling animation. So first the king
     *              will move to the new position and finally the rook will
     *              move.
     */
    public void moveFigure(FieldCoord fromCoord, FieldCoord toCoord, 
            FieldCoord hitFigureCoord, boolean silent, double delay) {
                
        System.out.println("CHESSBOARD - move figure from " + fromCoord.getCoord() +
                " to " + toCoord.getCoord() + " [hit figure coord = " + 
                hitFigureCoord + "]" + (silent ? " silently" : ""));
        
        Field from = getField(fromCoord);
        Field to = getField(toCoord);
        
        Figure figure = from.unbindFigure();
        
        // is needed to bring the figure to front (z order)
        figure.toFront();
        
        double deltaX = getFieldDeltaX(from, to);
        double deltaY = getFieldDeltaY(from, to);

        double avgDuration = getAvgDuration(from, to);
        
        Duration totalDuration = new Duration(avgDuration);
        
        TranslateTransition translateTransition = new TranslateTransition(totalDuration, figure);
        
        translateTransition.setToX(deltaX);
        translateTransition.setToY(deltaY);

        translateTransition.setOnFinished(event ->  {
            /**
             * alternative way:
             * ----------------
             * figure.setX(figure.getX() +  figure.getTranslateX());
             * figure.setY(figure.getY() +  figure.getTranslateY());            
             */
            figure.setX(to.getDrawX());
            figure.setY(to.getDrawY());
            figure.setTranslateX(0);
            figure.setTranslateY(0);            
        });
        
        translateTransition.setInterpolator(Interpolator.EASE_BOTH);

        ScaleTransition scaleTransition = new ScaleTransition(totalDuration.divide(2), figure);
        scaleTransition.setToX(TRANS_MOVE_SCALE_FACTOR);
        scaleTransition.setToY(TRANS_MOVE_SCALE_FACTOR);
        scaleTransition.setCycleCount(2);
        scaleTransition.setAutoReverse(true);
        
        scaleTransition.setInterpolator(Interpolator.LINEAR);
        
        ParallelTransition parallelTransition = new ParallelTransition(translateTransition, scaleTransition);

        if (hitFigureCoord != null) {
            
            Field hitFigureField = getField(hitFigureCoord);
            Figure hitFigure = hitFigureField.unbindFigure();
            
            FadeTransition hitFadeTransition = new FadeTransition(totalDuration.divide(3), hitFigure);            
            ScaleTransition hitScaleTransition = new ScaleTransition(totalDuration.divide(3), hitFigure);            
            hitFadeTransition.setDelay(totalDuration.multiply(2/3));
            hitScaleTransition.setDelay(totalDuration.multiply(2/3));
            
            hitFadeTransition.setToValue(0.0);
            hitScaleTransition.setToX(0.0);
            hitScaleTransition.setToY(0.0);
            
            ParallelTransition hitParallelTransition = new ParallelTransition(parallelTransition, 
                    hitFadeTransition, hitScaleTransition);
            
            hitParallelTransition.setDelay(new Duration(delay));
            hitParallelTransition.play();
            
        } else {
        
            parallelTransition.setDelay(new Duration(delay));
            parallelTransition.play();
            
        }
        
        to.bindFigure(figure);
        
    }   
    
}
