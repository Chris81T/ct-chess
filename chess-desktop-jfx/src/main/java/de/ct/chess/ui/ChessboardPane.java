/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.ui;

import javafx.beans.value.ChangeListener;
import javafx.scene.layout.StackPane;

/**
 *
 * @author christian
 */
public class ChessboardPane extends StackPane {
    
    private Chessboard chessboard = new Chessboard();

    public ChessboardPane() {
        getChildren().add(chessboard);
        
        getStyleClass().add("chessboard-pane");

        ChangeListener<Number> resizeListener = getResizeListener();
        widthProperty().addListener(resizeListener);
        heightProperty().addListener(resizeListener);
    }
    
    public Chessboard getChessboard() {
        return chessboard;
    }
    
    private ChangeListener<Number> getResizeListener() {
        return (observable, oldValue, newValue) -> {
            
            final double maxBoardSize = chessboard.getMaxBoardSize();
            
            final double curWidth = getWidth();
            final double curHeight = getHeight();
            
            final double curRefSize = Math.min(curWidth, curHeight);
            final double newScale = curRefSize / maxBoardSize;
            
            chessboard.setScale(newScale);
        };
    }
    
}
