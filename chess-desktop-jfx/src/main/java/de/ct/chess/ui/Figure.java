/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.ui;

import de.ct.chess.helpers.enums.Color;
import de.ct.chess.helpers.enums.FigureType;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

/**
 *
 * @author christian
 */
public class Figure extends ImageView {
   
    private final FigureType figureType;
    private final Color color;
    
    private final Image image;
    
    private Field field = null;
    
    public Figure(FigureType figureType, Color color, double size) {
        this.figureType = figureType;
        this.color = color;
        image = loadImage();
        setImage(image);
        setFitWidth(size);
        setPreserveRatio(true);
        setSmooth(true);
        //setCache(true); <-- no more good quality with this property
    }
    
    private Image loadImage() {
        
        StringBuilder path = new StringBuilder("figures/wiki/");
        
        switch (figureType) {
            case KING:
                path.append('k');
                break;
            case QUEEN:
                path.append('q');
                break;
            case BISHOP:
                path.append('b');
                break;
            case KNIGHT:
                path.append('n');
                break;
            case ROOK:
                path.append('r');
                break;
            case PAWN:
                path.append('p');
                break;
        }
        
        switch (color) {
            case BLACK:
                path.append('d');
                break;
            case WHITE:
                path.append('l');
                break;
        }
        
        path.append("t.png");
        
        return new Image(path.toString());        
    }

    public FigureType getFigureType() {
        return figureType;
    }

    public Color getColor() {
        return color;
    }
    
    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public void unbindField() {
        setField(null);
    }

    /**
     * Manual created toString method! Be aware of auto generation here. Cyclic
     * references with the Field toString.
     */
    @Override
    public String toString() {
        return "Figure{" + "figureType=" + figureType + ", color=" + color + 
                ", image=" + image + ", field=" + (field != null ? field.getCoord() : "unknown field?") + '}';
    }
    
}
