/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.ui;

import de.ct.chess.exceptions.ChessFxRuntimeException;
import de.ct.chess.helpers.objects.FieldCoord;
import de.ct.chess.ui.enums.FieldType;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.scene.Group;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 *
 * @author christian
 */
public class Field extends Group {
    
    private static final double SIGNAL_PATH_LENGTH = 15.0d;
    private static final double SIGNAL_PATH_MARGIN = 6.0d;
    
    private static final double TRANS_FADE_SIGNAL_LINES_DURATION = 100;
    private static final double TRANS_FADE_FIELD_WARNING = 200;
    
    private final Rectangle fieldBase;
    private final Path signalisationLinesPath;
    private final Rectangle fieldWarning;
    
    private final FieldType fieldType;
    private final FieldCoord coord;
    
    private Figure figure = null;
    
    public Field(int numX, int numY, double drawX, double drawY, int fieldSize, FieldType fieldType) {

        this.fieldType = fieldType;
        coord = new FieldCoord(numX, numY);

        fieldBase = new Rectangle(drawX, drawY, fieldSize, fieldSize);              
        fieldBase.getStyleClass().add(fieldType.getCssClass());        
        
        signalisationLinesPath = new Path();
        
        final double xOffset = drawX + SIGNAL_PATH_MARGIN;
        final double yOffset = drawY + SIGNAL_PATH_MARGIN;
        
        final double fieldSizeOffset = fieldSize - 2 * SIGNAL_PATH_MARGIN;
        
        signalisationLinesPath.getElements().addAll(
            new MoveTo(xOffset, yOffset + SIGNAL_PATH_LENGTH), 
                new LineTo(xOffset, yOffset), 
                new LineTo(xOffset + SIGNAL_PATH_LENGTH, yOffset),
                
            new MoveTo(xOffset + fieldSizeOffset, yOffset + SIGNAL_PATH_LENGTH), 
                new LineTo(xOffset + fieldSizeOffset, yOffset), 
                new LineTo(xOffset + fieldSizeOffset - SIGNAL_PATH_LENGTH, yOffset),

            new MoveTo(xOffset, yOffset + fieldSizeOffset - SIGNAL_PATH_LENGTH), 
                new LineTo(xOffset, yOffset + fieldSizeOffset), 
                new LineTo(xOffset + SIGNAL_PATH_LENGTH, yOffset + fieldSizeOffset),

            new MoveTo(xOffset + fieldSizeOffset, yOffset + fieldSizeOffset - SIGNAL_PATH_LENGTH), 
                new LineTo(xOffset + fieldSizeOffset, yOffset + fieldSizeOffset), 
                new LineTo(xOffset + fieldSizeOffset - SIGNAL_PATH_LENGTH, yOffset + fieldSizeOffset)
                
        );
        
        signalisationLinesPath.getStyleClass().add("field-signalisation-path");
        signalisationLinesPath.setVisible(false);
        signalisationLinesPath.setDisable(false);
        signalisationLinesPath.setOpacity(0d);
        
        fieldWarning = new Rectangle(drawX, drawY, fieldSize, fieldSize);
        fieldWarning.getStyleClass().add("field-warning");
        fieldWarning.setVisible(false);
        fieldWarning.setDisable(false);
        fieldWarning.setOpacity(0d);
        
        getChildren().addAll(fieldBase, fieldWarning, signalisationLinesPath);
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public int getNumX() {
        return coord.getNumX();
    }

    public int getNumY() {
        return coord.getNumY();
    }

    public double getDrawX() {
        return fieldBase.getX();
    }
    
    public double getDrawY() {
        return fieldBase.getY();
    }
    
    public double getDrawTranslateX() {
        return fieldBase.getTranslateX();
    }
    
    public double getDrawTranslateY() {
        return fieldBase.getTranslateY();
    }
    
    public String getFieldX() {
        return coord.getX();
    }

    public String getFieldY() {
        return coord.getY();
    }

    public String getCoord() {
        return coord.getCoord();
    }
    
    public FieldCoord getFieldCoord() {
        return coord;
    }

    void bindFigure(Figure figure) {
        this.figure = figure;
        figure.setField(this);
    }
    
    Figure unbindFigure() {
        if (figure != null) {
            figure.unbindField();
            Figure unboundFigure = figure;
            figure = null;
            return unboundFigure;
        } else {
            throw new ChessFxRuntimeException("Actually no figure is bound "
                    + "to field " + getCoord());
        }
    }

    public Figure getFigure() {
        return figure;
    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }
    
    public boolean isEmpty() {
        return getFigure() == null;
    }
    
    public boolean isOccupied() {
        return !isEmpty();
    }   
    
    public FadeTransition buildFadeInTransition() {
        FadeTransition fadeTransition = new FadeTransition(new Duration(TRANS_FADE_SIGNAL_LINES_DURATION), 
                signalisationLinesPath);        
        
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);
        
        fadeTransition.setOnFinished(event -> {            
            signalisationLinesPath.setVisible(true);
            signalisationLinesPath.setDisable(true);
        });
            
        signalisationLinesPath.toFront();
        
        return fadeTransition;
    }
    
    public FadeTransition buildFadeOutTransition() {
        FadeTransition fadeTransition = new FadeTransition(new Duration(TRANS_FADE_SIGNAL_LINES_DURATION), 
                signalisationLinesPath);        
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        
        fadeTransition.setOnFinished(event -> {            
            signalisationLinesPath.setVisible(false);
            signalisationLinesPath.setDisable(false);       
        });
        
        return fadeTransition;
    }
    
    public void markAsPossibleFieldToMove() {
        buildFadeInTransition().play();
    }
    
    public void unmarkAsPossibleFieldToMove() {
        buildFadeOutTransition().play();
    }

    public void highlightImpossibleMove() {
        fieldWarning.setVisible(true);
        fieldWarning.setDisable(true);            
        
        fieldWarning.toFront();
        
        FadeTransition fadeTransition = new FadeTransition(new Duration(TRANS_FADE_FIELD_WARNING / 2), 
                fieldWarning);        
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);
        fadeTransition.setAutoReverse(true);
        fadeTransition.setCycleCount(2);
        fadeTransition.setInterpolator(Interpolator.EASE_BOTH);
        
        fadeTransition.setOnFinished(event -> {            
            fieldWarning.setVisible(false);
            fieldWarning.setDisable(false);            
        });
        
        fadeTransition.play();
                
    }

    
    /**
     * Manual created toString method! Be aware of auto generation here. Cyclic
     * references with the Figure toString.
     */
    @Override
    public String toString() {
        return "Field{" + "fieldType=" + fieldType + ", coord=" + coord + 
                ", figure=" + 
                (figure != null ? figure.getFigureType() : "no figure set!") + '}';
    }

}
