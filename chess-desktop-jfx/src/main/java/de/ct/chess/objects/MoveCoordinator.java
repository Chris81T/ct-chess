/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.objects;

import de.ct.chess.adapter.ChessEngineAdapter;
import de.ct.chess.adapter.objects.GameHandle;
import de.ct.chess.adapter.objects.MoveToResult;
import de.ct.chess.events.PossibleMovesEvent;
import de.ct.chess.events.UnmarkPossibleMovesEvent;
import de.ct.chess.helpers.objects.FieldCoord;
import de.ct.chess.objects.enums.MoveCoordState;
import de.ct.chess.ui.Chessboard;
import de.ct.chess.ui.Field;
import de.ct.chess.ui.Figure;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * It works like a state machine:
 * 
 * Some component of the application like a MouseClick Event is able to trigger
 * a new movement, using the @see Game instance (should be known by the event).
 * 
 * 1.   NEW_MOVEMENT
 * 2.   POSSIBLE FIELDS
 * 3.   SOMEONE WILL CHECK, IF GIVEN FIELD IS ONE OF THE POSSIBLE ONE'S
 * 4.   FINALLY THE MOVEMENT WILL BE PERFORMED
 * 5.   THE UI WILL BE UPDATED (IS DIFFERENT FOR DRAG N DROP OR CLICK N CLICK)
 * 6.   FOR SOME SPECIAL OPERATION THIS INSPECTOR WILL 
 * 7.   THE MOVEMENT WILL BE COMPLETED
 * 
 * @author christian
 */
public class MoveCoordinator {
    
    private final GameHandle handle;
    private final ChessEngineAdapter chessEngine;
    private final Chessboard chessboard;
    
    private final FieldCoord from;
    private final List<FieldCoord> possibleMoves;
    
    private final MoveCoordinator lastCoordinator;
    
    private MoveCoordState state = MoveCoordState.ACTIVE;
    
    public MoveCoordinator(ChessEngineAdapter chessEngine, GameHandle handle, 
            Chessboard chessboard, FieldCoord from, MoveCoordinator lastCoordinator) {
        this.chessEngine = chessEngine;
        this.handle = handle;
        this.chessboard = chessboard;
                
        this.from = from;
        this.possibleMoves = chessEngine.possibleMoves(handle, from);
        this.lastCoordinator = lastCoordinator;
    }

    public MoveCoordState getState() {
        return state;
    }
    
    /**
     * @return a list of possible moves or an empty list. If it is an empty list,
     *         no movement is possible for the figure at the from field 
     *         coordinate.
     */
    public List<FieldCoord> getPossibleMoves() {
        return possibleMoves;                
    }
 
    public boolean isMovementPossible() {
        return !possibleMoves.isEmpty();
    }
    
    public boolean isPartOfPossibleMoves(FieldCoord toCheck) {
        return possibleMoves.contains(toCheck);
    }
    
    public void startMovementRequest(Figure figure) {

        if (lastCoordinator != null) {
            List<FieldCoord> lastPossibleMoves = lastCoordinator.getPossibleMoves();

            List<FieldCoord> movesToCancel = new ArrayList<>();
            List<FieldCoord> movesAlive = new ArrayList<>();
            
            lastPossibleMoves.forEach(lastCoord -> {
                if (possibleMoves.contains(lastCoord)) {
                    movesAlive.add(lastCoord);
                } else {
                    movesToCancel.add(lastCoord);
                }
            });
            
            List<FieldCoord> newRelevantMoves = possibleMoves.stream()
                    .filter(moveCoord -> !movesAlive.contains(moveCoord))
                    .collect(Collectors.toList());
            
            fireUnmarkPossibleMovesEvent(movesToCancel);
            firePossibleMovesEvent(newRelevantMoves, figure);            
        } else {
            firePossibleMovesEvent(possibleMoves, figure);            
        }
        
    }
    
    /**
     * 
     * @param to is the destination field
     * @param silent is relevant for animation. If it is set to true, no animation
     *               will be performed. Useful for the drag and drop mechanism.
     */
    public void performMoveTo(FieldCoord to, boolean silent) {
        MoveToResult moveToResult = chessEngine.moveTo(handle, from, to);
            
        chessboard.moveFigure(from, to, moveToResult.getHitFigureCoord(), silent);

        fireUnmarkPossibleMovesEvent(possibleMoves);
        
        if (moveToResult.isCastlingPerformed()) {
            
            final String yCoord = handle.isWhiteTheActivePlayer() ? "1" : "8";
            final double lastAvgDuration = chessboard.getAvgDuration(from, to);
            
            switch (moveToResult.getCastlingType()) {
                case KINGSIDE_CASTLING:
                    chessboard.moveFigure(new FieldCoord("h", yCoord), new FieldCoord("f", yCoord), 
                            null, silent, lastAvgDuration);
                    break;
                case QUEENSIDE_CASTLING:
                    chessboard.moveFigure(new FieldCoord("a", yCoord), new FieldCoord("d", yCoord), 
                            null, silent, lastAvgDuration);
                    break;
            }
            
        } else if (moveToResult.isPawnTransformationPossible()) { 
            // TODO pawn transform dialog 
            System.out.println("TODO TODO --> check for special operation like pawn transformation");
        }

        chessEngine.completeMoveTo(handle, moveToResult);
        
        state = MoveCoordState.FINISHED;
    }

    public void performMoveTo(Field field) {
        performMoveTo(field.getFieldCoord(), false);
    }  
    
    public void firePossibleMovesEvent(List<FieldCoord> possibleMoves, Figure figure) {
        possibleMoves.stream()
            .forEach(possibleMove -> {
                Field field = chessboard.getField(possibleMove);
                PossibleMovesEvent possibleMovesEvent = new PossibleMovesEvent(this, figure, field);
                field.fireEvent(possibleMovesEvent);                                        
            });
    }

    public void fireUnmarkPossibleMovesEvent(List<FieldCoord> fieldsToUnmark) {
        fieldsToUnmark.stream()
            .forEach(possibleMove -> {
                Field field = chessboard.getField(possibleMove);
                UnmarkPossibleMovesEvent unmarkPossibleMovesEvent = new UnmarkPossibleMovesEvent(this, field);
                field.fireEvent(unmarkPossibleMovesEvent);                                        
            });
    }

    void unmarkExistingPossibleFields() {
        fireUnmarkPossibleMovesEvent(possibleMoves);
    }
    
}
