/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.objects;

import de.ct.chess.adapter.ChessEngineAdapter;
import de.ct.chess.adapter.objects.GameHandle;
import de.ct.chess.helpers.enums.Color;
import de.ct.chess.helpers.objects.FieldCoord;
import de.ct.chess.objects.enums.MoveCoordState;
import de.ct.chess.ui.Chessboard;
import de.ct.chess.ui.Field;
import java.util.Objects;

/**
 * Is useful to give the different components the relevant module references
 * 
 * IMPORTANT:
 * 
 * Only this class will create a new instance of ChessEngineAdapter and will 
 * hold every relevant features for one specific game. 
 * For instance the movement logic / binding to the engine will be handled via
 * this game class/ to runtime the instance for the game. This boost the
 * performance, while multiple calls using the chess-engine can be saved.
 * 
 * @author christian
 */
public class Game {
    
    private final Chessboard chessboard;
    private final GameHandle handle;
    private final ChessEngineAdapter chessEngine;
    
    private MoveCoordinator moveCoordinator = null;
    
    public Game(Chessboard chessboard) {
        this.chessboard = chessboard;
        this.chessEngine = new ChessEngineAdapter();
        this.handle = chessEngine.newGame();
    }
    
    public Chessboard getChessboard() {
        return chessboard;
    }

    public GameHandle getHandle() {
        return handle;
    }

    public ChessEngineAdapter getChessEngine() {
        return chessEngine;
    }
        
    public boolean movementRequest(FieldCoord from) {
        
        MoveCoordinator lastCoordinator = moveCoordinator;
        
        MoveCoordinator coordinator = new MoveCoordinator(chessEngine, handle, 
                chessboard, from, lastCoordinator);
        if (coordinator.isMovementPossible()) {
            moveCoordinator = coordinator;
            return true;
        }
        /**
         * must not be set to null here. Else during the next movementRequest the
         * lastCoordinator will be set with null. That will be wrong.
         */
        // moveCoordinator = null;
        return false;
    }

    /**
     * typically the @see movementRequest method has returned false. So the client
     * should call this method to highlight the impossible move and check, that
     * the possible moves highlighting before will be hidden.
     * @param sourceCoord 
     */
    public void highlightImpossibleMove(FieldCoord sourceCoord) {
        if (moveCoordinator != null) {            
            moveCoordinator.unmarkExistingPossibleFields();
        }
        
        Field field = chessboard.getField(sourceCoord);
        field.highlightImpossibleMove();
    }

    public boolean isAPossibleFieldForMove(Field field) {
        return moveCoordinator != null &&
            moveCoordinator.getState().equals(MoveCoordState.ACTIVE) &&
            moveCoordinator.isPartOfPossibleMoves(field.getFieldCoord());
    }
    
    public MoveCoordinator getMoveCoordinator() {
        return moveCoordinator;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.chessboard);
        hash = 61 * hash + Objects.hashCode(this.handle);
        hash = 61 * hash + Objects.hashCode(this.chessEngine);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Game other = (Game) obj;
        if (!Objects.equals(this.chessboard, other.chessboard)) {
            return false;
        }
        if (!Objects.equals(this.handle, other.handle)) {
            return false;
        }
        if (!Objects.equals(this.chessEngine, other.chessEngine)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Game{" + "chessboard=" + chessboard + ", handle=" + handle 
                + ", chessEngine=" + chessEngine + '}';
    }

    
}
