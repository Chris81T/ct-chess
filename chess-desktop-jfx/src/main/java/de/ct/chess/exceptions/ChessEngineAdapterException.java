/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.exceptions;

/**
 *
 * @author p0514551
 */
public class ChessEngineAdapterException extends RuntimeException {

    public ChessEngineAdapterException(String message) {
        super(message);
    }

    public ChessEngineAdapterException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
