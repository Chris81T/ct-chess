/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.exceptions;

/**
 *
 * @author christian
 */
public class ChessFxRuntimeException extends RuntimeException {

    public ChessFxRuntimeException(String message) {
        super(message);
    }

    public ChessFxRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
