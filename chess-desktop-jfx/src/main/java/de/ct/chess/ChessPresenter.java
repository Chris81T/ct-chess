/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess;

import de.ct.chess.events.PossibleMovesEvent;
import de.ct.chess.events.UnmarkPossibleMovesEvent;
import de.ct.chess.events.handler.MouseDragAndDropEventHandler;
import de.ct.chess.events.handler.MouseEventHandler;
import de.ct.chess.events.handler.PossibleMovesEventHandler;
import de.ct.chess.helpers.objects.FieldCoord;
import de.ct.chess.ui.Chessboard;
import de.ct.chess.ui.Field;
import de.ct.chess.ui.Figure;
import de.ct.chess.helpers.enums.Color;
import de.ct.chess.helpers.enums.FigureType;
import de.ct.chess.objects.Game;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author christian
 */
public class ChessPresenter {

    private final Chessboard chessboard;

    private Game game = null;
    
    private MouseEventHandler mouseEventHandler = null;
    private MouseDragAndDropEventHandler mouseDragDropEventHandler = null;
    
    private PossibleMovesEventHandler possibleMovesEventHandler = null;
    
    public ChessPresenter(Chessboard chessboard) {
        this.chessboard = chessboard;
    }
    
    public void newGame() {

        game = new Game(chessboard);
        
        placeFiguresForNewGame();
        
    }
    
    private void initWhiteFigures() {
        chessboard.placeFigure(FigureType.ROOK, Color.WHITE, new FieldCoord("a1"));
        chessboard.placeFigure(FigureType.KNIGHT, Color.WHITE, new FieldCoord("b1"));
        chessboard.placeFigure(FigureType.BISHOP, Color.WHITE, new FieldCoord("c1"));
        chessboard.placeFigure(FigureType.QUEEN, Color.WHITE, new FieldCoord("d1"));
        chessboard.placeFigure(FigureType.KING, Color.WHITE, new FieldCoord("e1"));
        chessboard.placeFigure(FigureType.BISHOP, Color.WHITE, new FieldCoord("f1"));
        chessboard.placeFigure(FigureType.KNIGHT, Color.WHITE, new FieldCoord("g1"));
        chessboard.placeFigure(FigureType.ROOK, Color.WHITE, new FieldCoord("h1"));
        
        IntStream.rangeClosed(0, 7).forEach(x -> chessboard.placeFigure(FigureType.PAWN, Color.WHITE, new FieldCoord(x, 1)));        
    }
    
    private void initBlackFigures() {
        chessboard.placeFigure(FigureType.ROOK, Color.BLACK, new FieldCoord("a8"));
        chessboard.placeFigure(FigureType.KNIGHT, Color.BLACK, new FieldCoord("b8"));
        chessboard.placeFigure(FigureType.BISHOP, Color.BLACK, new FieldCoord("c8"));
        chessboard.placeFigure(FigureType.QUEEN, Color.BLACK, new FieldCoord("d8"));
        chessboard.placeFigure(FigureType.KING, Color.BLACK, new FieldCoord("e8"));
        chessboard.placeFigure(FigureType.BISHOP, Color.BLACK, new FieldCoord("f8"));
        chessboard.placeFigure(FigureType.KNIGHT, Color.BLACK, new FieldCoord("g8"));
        chessboard.placeFigure(FigureType.ROOK, Color.BLACK, new FieldCoord("h8"));
        
        IntStream.rangeClosed(0, 7).forEach(x -> chessboard.placeFigure(FigureType.PAWN, Color.BLACK, new FieldCoord(x, 6)));        
    }
    
    private void prepareMouseEventHandler(List<Figure> figures, List<Field> fields) {        
        
        mouseDragDropEventHandler = new MouseDragAndDropEventHandler(game);
        mouseEventHandler = new MouseEventHandler(game);
        
        fields.parallelStream().forEach(field -> {
            field.setOnDragOver(mouseDragDropEventHandler::fieldOnDragOver);
            field.setOnDragDropped(mouseDragDropEventHandler::fieldOnDragDropped);
            field.setOnDragEntered(mouseDragDropEventHandler::fieldOnDragEntered);
            field.setOnDragExited(mouseDragDropEventHandler::fieldOnDragExited); 
            
            field.setOnMouseEntered(mouseEventHandler::fieldOnMouseEntered);
            field.setOnMouseExited(mouseEventHandler::fieldOnMouseExited);
            field.setOnMouseClicked(mouseEventHandler::fieldOnMouseClicked);
        });
        
        figures.parallelStream().forEach(figure -> {
            figure.setOnMouseEntered(mouseEventHandler::figureOnMouseEntered);
            figure.setOnMouseExited(mouseEventHandler::figureOnMouseExited);
            figure.setOnMouseClicked(mouseEventHandler::figureOnMouseClicked);
            
            figure.setOnDragDetected(mouseDragDropEventHandler::figureOnDragDetected);
            figure.setOnDragDone(mouseDragDropEventHandler::figureOnDragDone);
        });
        
        
    }
    
    private void prepareCommonEventHandler(List<Figure> figures, List<Field> fields) {        
        
        possibleMovesEventHandler = new PossibleMovesEventHandler(game);
        
        fields.parallelStream().forEach(field -> {
            field.addEventHandler(PossibleMovesEvent.POSSIBLE_MOVES, 
                    possibleMovesEventHandler::fieldOnPossibleMoves);            
            field.addEventHandler(UnmarkPossibleMovesEvent.UNMARK_POSSIBLE_MOVES, 
                    possibleMovesEventHandler::fieldOnUnmarkPossibleMoves);            
        });
        
        figures.parallelStream().forEach(figure -> {

        });        
        
    }
    
    private void placeFiguresForNewGame() {        
        initWhiteFigures();
        initBlackFigures();
        
        List<Figure> figures = chessboard.getFigures();
        List<Field> fields = chessboard.getFields();
        
        prepareCommonEventHandler(figures, fields);

        // TODO check, if mouse input is given!
        prepareMouseEventHandler(figures, fields);
    }
            
}
