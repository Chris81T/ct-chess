package de.ct.chess;

import de.ct.chess.ui.ChessboardPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author christian
 */
public class ChessApplication extends Application {
    
    private static final String VERSION = "0.1";
    
    private ChessPresenter chessPresenter = null;
    
    @Override
    public void start(Stage primaryStage) {
        System.out.println("starting ctchess java fx edition");
        
        ChessboardPane chessboardPane = new ChessboardPane();               
        
        chessPresenter = new ChessPresenter(chessboardPane.getChessboard());
        
        // TODO: check the initially size setup. Maybe store the last set window size
        Scene scene = new Scene(chessboardPane, 1280, 1024);
        scene.getStylesheets().add("stylesheets/chessboard.css");
                
        primaryStage.setTitle("ct Chess " + VERSION);        
        primaryStage.setScene(scene);

        // primaryStage.setAlwaysOnTop(true);
        primaryStage.setOnCloseRequest(event -> {
            System.out.println("ON_CLOSE event is requested. Event type = " + event.getEventType());
        });
        
        chessPresenter.newGame();
        
        primaryStage.show();
    }

    @Override
    public void stop() {
        System.out.println("stopping c-board");
    }
    
    public static void main(String... args) {
        launch(args);
    }
    
}
