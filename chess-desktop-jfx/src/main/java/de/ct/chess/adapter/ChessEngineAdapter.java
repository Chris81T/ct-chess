/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.adapter;

import de.ct.chess.adapter.objects.GameState;
import de.ct.chess.adapter.objects.MoveToResult;
import de.ct.chess.adapter.objects.GameHandle;
import de.ct.chess.adapter.objects.enums.GameStateType;
import de.ct.chess.exceptions.ChessEngineAdapterException;
import de.ct.chess.helpers.objects.FieldCoord;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 *
 * @author christian
 */
public class ChessEngineAdapter {
   
    private static final String JS_LOCATION_PATH = "/chessengine/";
    private static final String JS_LOCATION_FILE = JS_LOCATION_PATH + "nashorn.js";
    
    private static final String INIT_INVOKABLE_FUNC = "createChessEngine";
        
    private final ScriptEngine scriptEngine;
    private final ChessEngineConnector chessEngine;
        
    public ChessEngineAdapter() throws ChessEngineAdapterException {
        try {
            InputStream sampleStream = getClass().getResourceAsStream(JS_LOCATION_FILE);
            
            scriptEngine = new ScriptEngineManager().getEngineByName("nashorn");
            scriptEngine.eval(new InputStreamReader(sampleStream));
            Invocable chessEngineInvocable = (Invocable) scriptEngine;

            chessEngine = chessEngineInvocable.getInterface(ChessEngineConnector.class);
            
        } catch (ScriptException e) {
            throw new ChessEngineAdapterException("Something went wrong with nashorn!", e);
        } catch (/*NoSuchMethod*/Exception e) {
            throw new ChessEngineAdapterException("Could not call relevant function with name " 
                    + INIT_INVOKABLE_FUNC, e);
        }
        
    }
 
    public GameHandle newGame() {
        return new GameHandle(chessEngine.newGame());
    }
    
    public GameHandle loadGame(String savedGame) {
        throw new UnsupportedOperationException("actual missing feature!");
    }
    
    public String saveGame(GameHandle handle) {
        throw new UnsupportedOperationException("actual missing feature!");        
    }
    
    public MoveToResult moveTo(GameHandle handle, FieldCoord from, FieldCoord to) {
        return new MoveToResult(chessEngine.moveTo(handle.getJsHandle(), from.getCoord(), to.getCoord()));
    }
    
    public GameState completeMoveTo(GameHandle handle, MoveToResult moveToResult) {
        return new GameState(GameStateType.valueOf(chessEngine.completeMoveTo(handle.getJsHandle(), moveToResult.getJsHandle())));        
    }
    
    public List<FieldCoord> possibleMoves(GameHandle handle, FieldCoord from) {        
        ScriptObjectMirror result = chessEngine.possibleMoves(handle.getJsHandle(), from.getCoord());
        List<ScriptObjectMirror> moves = Arrays.asList(result.to(ScriptObjectMirror[].class));

        return moves.stream()
            .map(ChessEngineAdapter::parseFieldCoord)
            .collect(Collectors.toList());
    }
    
    /**
     * TODO: strange, that y will be recognized as a double value. Check it...
     * 
     * Sub 1 from each coordinate. The js engine works with 1 - 8. 
     * The FieldCoord with 0 - 7
     */
    public static FieldCoord parseFieldCoord(ScriptObjectMirror move) {
        if (move == null) return null;
        final int numX = (int) move.get("x") - 1;
        final int numY = (int)(double) move.get("y") - 1;
        return new FieldCoord(numX, numY);        
    }
    
    
    /**
     * TODO could be a test class!
     * @param args 
     */
    public static void main(String... args) {
        
        System.out.println("############### TEST NASHORN....");
        
        ChessEngineAdapter adapter = new ChessEngineAdapter();
 
        GameHandle gameHandle = adapter.newGame();
        
        List<FieldCoord> moves = adapter.possibleMoves(gameHandle, new FieldCoord("c2"));
        
        System.out.println("test result: " + moves);
        
        MoveToResult moveToResult = adapter.moveTo(gameHandle, new FieldCoord("c2"), new FieldCoord("c4"));
        
        System.out.println("moveTo result = " + moveToResult);
        
        GameState state = adapter.completeMoveTo(gameHandle, moveToResult);
        
        System.out.println("game state = " + state);
        
        // not allowed call. Black player is active
        //MoveToResult moveToResult2 = adapter.moveTo(gameHandle, new FieldCoord("c4"), new FieldCoord("c5"));
        
    }
    
}
