/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.adapter.objects;

import java.util.Objects;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 *
 * @author christian
 */
public abstract class AbstractGameObject {
    
    private final ScriptObjectMirror handle;

    public AbstractGameObject(ScriptObjectMirror handle) {
        this.handle = handle;
    }

    /**
     * access for the concrete implementation
     * @param key for desired value
     */
    protected <T> T getValue(final String key) {
        return (T) handle.get(key);
    }
    
    protected <T> T getInnerValue(final String key, final String innerKey) {
        return (T) getRawValue(key).get(innerKey);
    }
    
    protected ScriptObjectMirror getRawValue(final String key) {
        return getValue(key);
    }
    
    public ScriptObjectMirror getJsHandle() {
        return handle;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.handle);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractGameObject other = (AbstractGameObject) obj;
        if (!Objects.equals(this.handle, other.handle)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractGameObject{" + "handle=" + handle + '}';
    }
    
}
