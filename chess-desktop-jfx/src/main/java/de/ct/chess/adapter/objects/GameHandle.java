/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.adapter.objects;

import de.ct.chess.helpers.enums.Color;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 *
 * @author christian
 */
public class GameHandle extends AbstractGameObject {
    
    public GameHandle(ScriptObjectMirror handle) {
        super(handle);
    }
    
    public Color getActivePlayer() {
        return Color.valueOf(getValue("activePlayer"));
    }
    
    public boolean isWhiteTheActivePlayer() {
        return getActivePlayer().equals(Color.WHITE);
    }
    
    public boolean isBlackTheActivePlayer() {
        return !isWhiteTheActivePlayer();
    }
    
}
