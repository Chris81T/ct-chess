/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.adapter;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 * Interface will be implemented by nashorn using the nashorn.js as implementation
 * 
 * @author christian
 */
public interface ChessEngineConnector {
    
    ScriptObjectMirror newGame();
    ScriptObjectMirror loadGame(ScriptObjectMirror savedGame);
    ScriptObjectMirror saveGame(ScriptObjectMirror handle);
    
    ScriptObjectMirror moveTo(ScriptObjectMirror handle, String from, String to);
    String completeMoveTo(ScriptObjectMirror handle, ScriptObjectMirror moveToResult);
    ScriptObjectMirror possibleMoves(ScriptObjectMirror handle, String from);
    
}
