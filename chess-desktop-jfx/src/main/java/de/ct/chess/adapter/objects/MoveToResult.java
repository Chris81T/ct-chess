/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.adapter.objects;

import de.ct.chess.adapter.ChessEngineAdapter;
import de.ct.chess.adapter.objects.enums.MoveToResultType;
import de.ct.chess.helpers.enums.Color;
import de.ct.chess.helpers.enums.PgnType;
import de.ct.chess.helpers.objects.FieldCoord;
import java.util.Objects;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 *
 * @author christian
 */
public class MoveToResult extends AbstractGameObject {
    
    private final MoveToResultType resultType;
    private final Color colorType;
    
    private final boolean pawnTransformationPossible;
    private final PgnType castlingType;
    
    private final FieldCoord from;
    private final FieldCoord to;
    private final FieldCoord hitFigureCoord;
    
    public MoveToResult(ScriptObjectMirror handle) {
        super(handle);
        
        resultType = MoveToResultType.valueOf(getValue("resultType"));
        colorType = Color.valueOf(getValue("colorType"));
        
        pawnTransformationPossible = getInnerValue("pawnTransformation", "possible");
        
        String castlingTypeValue = getValue("castlingType");
        castlingType = castlingTypeValue != null ? PgnType.getEnum(castlingTypeValue) : null;
        
        from = ChessEngineAdapter.parseFieldCoord(getValue("fromField"));
        to = ChessEngineAdapter.parseFieldCoord(getValue("toField"));
                
        hitFigureCoord = ChessEngineAdapter.parseFieldCoord(getValue("hitFigureField"));
    }

    public MoveToResultType getResultType() {
        return resultType;
    }

    public Color getColorType() {
        return colorType;
    }

    public boolean isPawnTransformationPossible() {
        return pawnTransformationPossible;
    }

    public PgnType getCastlingType() {
        return castlingType;
    }

    public boolean isCastlingPerformed() {
        return castlingType != null;
    }
    
    public FieldCoord getFrom() {
        return from;
    }

    public FieldCoord getTo() {
        return to;
    }

    public FieldCoord getHitFigureCoord() {
        return hitFigureCoord;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.resultType);
        hash = 97 * hash + Objects.hashCode(this.colorType);
        hash = 97 * hash + (this.pawnTransformationPossible ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.castlingType);
        hash = 97 * hash + Objects.hashCode(this.from);
        hash = 97 * hash + Objects.hashCode(this.to);
        hash = 97 * hash + Objects.hashCode(this.hitFigureCoord);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MoveToResult other = (MoveToResult) obj;
        if (this.pawnTransformationPossible != other.pawnTransformationPossible) {
            return false;
        }
        if (this.resultType != other.resultType) {
            return false;
        }
        if (this.colorType != other.colorType) {
            return false;
        }
        if (this.castlingType != other.castlingType) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.hitFigureCoord, other.hitFigureCoord)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MoveToResult{" + "resultType=" + resultType + ", colorType=" + 
                colorType + ", pawnTransformationPossible=" + pawnTransformationPossible +
                ", castlingType=" + castlingType + ", from=" + from + ", to=" + to 
                + ", hitFigureCoord=" + hitFigureCoord + '}';
    }
   
}