/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.events;

import de.ct.chess.objects.MoveCoordinator;
import de.ct.chess.ui.Field;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author christian
 */
public class UnmarkPossibleMovesEvent extends Event {
    
    public static final EventType<UnmarkPossibleMovesEvent> UNMARK_POSSIBLE_MOVES = new EventType<>(Event.ANY, "UNMARK_POSSIBLE_MOVES");
    
    public UnmarkPossibleMovesEvent(MoveCoordinator moveCoordinator) {
        super(UNMARK_POSSIBLE_MOVES);
    }

    // TODO source should be field!
    public UnmarkPossibleMovesEvent(MoveCoordinator moveCoordinator, Field target) {
        super(target, target, UNMARK_POSSIBLE_MOVES);
    }

    @Override
    public UnmarkPossibleMovesEvent copyFor(Object newSource, EventTarget newTarget) {
        return (UnmarkPossibleMovesEvent) super.copyFor(newSource, newTarget);
    }

    @Override
    public EventType<? extends UnmarkPossibleMovesEvent> getEventType() {
        return (EventType<? extends UnmarkPossibleMovesEvent>) super.getEventType();
    }

}
