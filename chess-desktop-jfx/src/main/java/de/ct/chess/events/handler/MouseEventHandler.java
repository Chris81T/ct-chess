/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.events.handler;

import de.ct.chess.adapter.objects.GameHandle;
import de.ct.chess.helpers.objects.FieldCoord;
import de.ct.chess.objects.Game;
import de.ct.chess.objects.MoveCoordinator;
import de.ct.chess.ui.Field;
import de.ct.chess.ui.Figure;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author christian
 */
public class MouseEventHandler extends AbstractGameHandler {
    
    public MouseEventHandler(Game game) {
        super(game);
    }

    private void startMovementRequest(Figure figure) {
        FieldCoord sourceCoord = figure.getField().getFieldCoord();
        
        if (isPlayerActive(figure)) {

            if (game.movementRequest(sourceCoord)) {
                MoveCoordinator moveCoordinator = game.getMoveCoordinator();
                moveCoordinator.startMovementRequest(figure);
            } else {
                game.highlightImpossibleMove(sourceCoord);
            }
            
        } else {
            
            System.out.println("/* NO MOVEMENT POSSIBLE - WRONG PLAYER - TODO TODO TDO - HIGHLIGHT OR SO???? */");            
            
        }
        
    }
    
    public void figureOnMouseEntered(MouseEvent event) {        
        Figure figure = (Figure) event.getSource();
        if (isPlayerActive(figure)) {
            figure.setCursor(Cursor.HAND);
        } 
        event.consume();
    }

    public void figureOnMouseExited(MouseEvent event) {        
        Figure figure = (Figure) event.getSource();
        figure.setCursor(Cursor.DEFAULT);        
        event.consume();
    }

    public void figureOnMouseClicked(MouseEvent event) {
        Figure figure = (Figure) event.getSource();
        System.out.println("mouse click on figure " + figure);
  
        startMovementRequest(figure);
        
        event.consume();
    }
        
    public void fieldOnMouseEntered(MouseEvent event) {        
        Field field = (Field) event.getSource();

        if (game.isAPossibleFieldForMove(field)) {
            field.setCursor(Cursor.HAND);            
        }
        
        event.consume();
    }

    public void fieldOnMouseExited(MouseEvent event) {        
        Field field = (Field) event.getSource();
        field.setCursor(Cursor.DEFAULT);        
        event.consume();
    }
 
    /**
     * Check, if this is a possible field of a still in progress existing 
     * movement. If not, check if an underlying figure exists. Then this can
     * be the beginning of a new movement. The MoveCoordinator has to decide.
     * 
     * @param event 
     */
    public void fieldOnMouseClicked(MouseEvent event) {
        Field field = (Field) event.getSource();
        
        GameHandle handle = game.getHandle();
        
        if (game.isAPossibleFieldForMove(field)) {
            
            MoveCoordinator moveCoordinator = game.getMoveCoordinator();
            moveCoordinator.performMoveTo(field);
            
        } else if (field.isOccupied() &&
            field.getFigure().getColor().equals(handle.getActivePlayer())) {
            
            startMovementRequest(field.getFigure());
            
        }
    }
    
}
