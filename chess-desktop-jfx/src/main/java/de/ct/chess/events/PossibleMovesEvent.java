/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.events;

import de.ct.chess.objects.MoveCoordinator;
import de.ct.chess.ui.Field;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author christian
 */
public class PossibleMovesEvent extends Event {
    
    public static final EventType<PossibleMovesEvent> POSSIBLE_MOVES = new EventType<>(Event.ANY, "POSSIBLE_MOVES");

    private final MoveCoordinator moveCoordinator;
    
    public PossibleMovesEvent(MoveCoordinator moveCoordinator) {
        super(POSSIBLE_MOVES);
        this.moveCoordinator = moveCoordinator;
    }

    // TODO source should be field!
    public PossibleMovesEvent(MoveCoordinator moveCoordinator, Object source, Field target) {
        super(source, target, POSSIBLE_MOVES);
        this.moveCoordinator = moveCoordinator;
    }

    @Override
    public PossibleMovesEvent copyFor(Object newSource, EventTarget newTarget) {
        return (PossibleMovesEvent) super.copyFor(newSource, newTarget);
    }

    @Override
    public EventType<? extends PossibleMovesEvent> getEventType() {
        return (EventType<? extends PossibleMovesEvent>) super.getEventType();
    }

    public MoveCoordinator getMoveCoordinator() {
        return moveCoordinator;
    }
    
}
