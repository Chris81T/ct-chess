/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.events.handler;

import de.ct.chess.events.PossibleMovesEvent;
import de.ct.chess.events.UnmarkPossibleMovesEvent;
import de.ct.chess.objects.Game;
import de.ct.chess.objects.MoveCoordinator;
import de.ct.chess.ui.Field;

/**
 *
 * @author christian
 */
public class PossibleMovesEventHandler extends AbstractGameHandler {

    public PossibleMovesEventHandler(Game game) {
        super(game);
    }
   
    public void fieldOnPossibleMoves(PossibleMovesEvent event) {      
        Field field = (Field) event.getSource();        
        
        MoveCoordinator moveCoordinator = event.getMoveCoordinator();
        if (moveCoordinator.isPartOfPossibleMoves(field.getFieldCoord())) {
            // signalize the user this possible field for movement
            field.markAsPossibleFieldToMove();
        }
     
        event.consume();
    }
    
    public void fieldOnUnmarkPossibleMoves(UnmarkPossibleMovesEvent event) {
        Field field = (Field) event.getSource();
        field.unmarkAsPossibleFieldToMove();
        
        event.consume();
    }
    
}
