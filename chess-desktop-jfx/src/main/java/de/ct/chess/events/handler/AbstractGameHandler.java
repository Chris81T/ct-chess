/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.events.handler;

import de.ct.chess.objects.Game;
import de.ct.chess.ui.Figure;

/**
 *
 * @author christian
 */
public abstract class AbstractGameHandler {
    
    public final Game game;

    public AbstractGameHandler(Game game) {
        this.game = game;
    }
    
    public boolean isPlayerActive(Figure figure) {
        return game.getHandle().getActivePlayer().equals(figure.getColor());
    }
    
}
