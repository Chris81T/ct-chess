/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ct.chess.events.handler;

import de.ct.chess.objects.Game;
import de.ct.chess.ui.Field;
import de.ct.chess.ui.Figure;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

/**
 *
 * @author christian
 */
public class MouseDragAndDropEventHandler extends AbstractGameHandler {

    public MouseDragAndDropEventHandler(Game game) {
        super(game);
    }
    
    public void figureOnDragDetected(MouseEvent event) {
        Figure figure = (Figure) event.getSource();
        
        if (isPlayerActive(figure)) {

            Dragboard dragboard = figure.startDragAndDrop(TransferMode.MOVE);

            ClipboardContent clipboard = new ClipboardContent();

            Image figureImage = figure.getImage();

            clipboard.putImage(figureImage);

            dragboard.setContent(clipboard);

            // the mouse movement is showing the image actually
            figure.setVisible(false);

        }
                
        event.consume();
    }
    
    public void fieldOnDragEntered(DragEvent event) {
        Field field = (Field) event.getSource();
        System.out.println("MouseDragAndDropEventHandler - on drag entered (source) :: " + event.getSource());
                
        event.consume();        
    }
    
    public void fieldOnDragExited(DragEvent event) {
        Field field = (Field) event.getSource();
        System.out.println("MouseDragAndDropEventHandler - on drag exited (source) :: " + event.getSource());
        
        event.consume();        
    }
    
    public void figureOnDragDone(DragEvent event) {
        Figure figure = (Figure) event.getSource();
        System.out.println("MouseDragAndDropEventHandler - on drag done (source) :: " + event.getSource());

        // drag is exited. Show the figure
        figure.setVisible(true);

        event.consume();
    }
    
    public void fieldOnDragOver(DragEvent event) {
        System.out.println("MouseDragAndDropEventHandler - on drag over (source) :: " + event.getSource());

        // TODO condition to check, that the source is a figure or so...
        event.acceptTransferModes(TransferMode.MOVE);

        event.consume();
    }
    
    public void fieldOnDragDropped(DragEvent event) {
        System.out.println("MouseDragAndDropEventHandler - on drag dropped (source) :: " + event.getSource());
        
        event.setDropCompleted(true);
        event.consume();
    }
    
}
